---
title: "Territoire Apprenant Contributif (Contributory Learning Territory)"
date: 2019-02-22
description: "The TAC program is a territorial research experiment which aims to develop a contributory economy."
header_transparent: true
hero:
  type: hero
  enabled: true
  options:
    paddingTop: ""
    paddingBottom: false
    borderTop: false
    borderBottom: false
    theme: primary
    classes: "my-custom-class another-custom-class"
  align_horizontal: left
  align_vertical: middle
  height: 600px
  fullscreen_mobile: true
  fullscreen_desktop: false
  headings:
    heading: Territoire Apprenant Contributif
    sub_heading: The Territoire Apprenant Contributif (TAC) program is a long-term contributory research program that brings together residents, professionals and researchers, in order to cultivate new knowledge practices within the framework of a contributory economy that supports sustainable bifurcations.
    text: ''
    text_color: "#FFFFFF"
    text_color_dark: "#FFFFFF"
  background:
    background_image: "images/photos/landing_background.jpg"
    background_image_blend_mode: "overlay" # "overlay", "multiply", "screen"
    background_gradient: true
    background_color: "" # "#030202"
    background_color_dark: "" # "#030202" 
    opacity: 1
  image:
    image: false
    shadow: false
    border: false
    alt: ""
  buttons:
    - button:
      text: "Find out more"
      url: "/programme/"
      external: false
      fa_icon: false
      outline: true
      style: "transparent"
axes:
  enabled: true
  show_view_all: true
  sort_by: "weight" # "date" "weight"
  limit: 3
  label: "Research axes"
intro:
  enabled: true
  align: left
  image: "images/photos/activities/pmi_background.jpeg"
  heading: "Promoting collective intelligence for more resilient territories"
  description: "The TAC program is made up of numerous activities based on the principles of contribution and the promotion of theoretical, practical, and lived knowledge."
  buttons:
    - button:
      text: "Discover our activities"
      url: "/activities/"
      external: false
      fa_icon: false
      outline: true
      style: "primary"
  partners:
    enabled: true
activities:
  enabled: true
  label: "Some activities"
  show_view_all: false
  limit: 6
outro:
  enabled: true
  align: center
  image: ""
  heading: Share your story, meet us, or contribute to our projects
  description: If you want to accelerate the social, ecological and economic transformation of your territory
  buttons:
    - button:
      text: "Contact us"
      url: "contact/"
      external: false
      fa_icon: false
      outline: false
      style: ""
blog:
  enabled: true
  show_view_all: false
  limit: 3
---
