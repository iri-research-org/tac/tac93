---
title: "Digital Urbanity in the Games #1"
description: "From school court yards to the construction of the olympic village, the Project UNEJ reimagined urban planning and develop pedagogy with educational establishments in Seine Saint-Denis."
period: 2019-2024
date: 2024-11-20
weight: 1
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Digital Urbanity in Game #1"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "From school court yards to the construction of the olympic village, the Project UNEJ reimagined urban planning and develop pedagogy with educational establishments in Seine Saint-Denis."

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/unej.png"
icon: ""

hero:
  headings:
    heading: "Digital Urbanity in Game #1"
    sub_heading: "From school court yards to the construction of the olympic village, the Project UNEJ reimagined urban planning and develop pedagogy with educational establishments in Seine Saint-Denis."
  background:
    background_image: "images/photos/activities/unej_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content:
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Cabinet of architecture and urbanism O’zone"
      link: http://www.ozone-architectures.com/
    - name: "ICI !, Construction initiatives from Îlo-Dionysiennes (association of architects)"
      link: https://www.associationici.com/
    - name: "3 Hit Combo Association, leader of the RennesCraft project"
      link: https://www.3hitcombo.fr/rennescraft/
    - name: "Regional education authority of the Academy of Créteil"
      link: https://www.ac-creteil.fr/
    - name: "IGN"
      link: https://www.ign.fr/
    - name: "CDOS 93 (Departmental Olympic and Sport Committee of Seine-Saint-Denis)"
      link: https://www.cdos93.org/
  text: "UNEJ capacitated teachers and their students to develop and present propositions to reimagine and redesign their schools and communities. With the free and open source version of Minecraft, 12 high schools and middle schools in Seine-Saint-Denis invented the future of their city and the legacy of the Olympic Village."
  link_label: Site du projet
  link_url: "https://unej.tac93.fr/"
  fundings: 
    - name: "Caisse des Dépôts et Consignation"
      link: https://www.caissedesdepots.fr/
    - name: "Solidéo"
      link: https://www.ouvrages-olympiques.fr/
    - name: "Ministère de l’éducation nationale"
      link: https://www.education.gouv.fr/
    - name: "Fondation de France"
      link: https://www.fondationdefrance.org/fr/
    - name: "Fondation SNCF"
      link: https://www.groupe-sncf.com/fr/engagements/mecenat-sponsoring/fondation
    - name: Département de la Seine Saint-Denis
      link: https://seinesaintdenis.fr/
    - name: Fondation NextInternet
      link: https://nlnet.nl/project/MinetestEdu/
---

The IRI and its partners question **the future of the city in the era of new building technologies and platforms**. Experiments on the city like the Rennecraft experiment – which used the Minetest video game and data from the Geographic Information Service for the city of Rennes – inspired us a lot.

Started in partnership with the Regional education authority of Créteil, the UNEJ workshops are developed with architects, town planners, teachers, researchers and video game enthusiasts. These sessions take place during school time and are led by volunteer and trained teachers, as well as by the team of stakeholders (architects, urban planners, and digital designers) who provide 3 to 6 interventions per establishment per year – over the course of 4 years (2020-2024). The sessions rely on the use of the videogame **[Luanti](https://www.luanti.org/)** (free version of Minecraft, formerly Minetest) to support students in the design and re-modeling of their territory. These proposals were integrated into **the dynamic planning of the Olympic and Paralympic Games** at the end of 2024.

![Students working in Minetest](/images/photos/blur/unej_text1.jpg)

Between 2020 and 2021, the students worked to re-imagine and redesign their playground. The following year, 2021-2022, they focused on their district's architectural layout. In the 2023-2024 school year, they designed proposals for the Athletes' village and the Media village, built into the the 2024 Olympic Games urban structure. Noticably, these efforts contributed to the design of Mail Finot, a space in the heart of the Olympic village. The students came to know their territory better, and discovered professions within areas such as town planneing, architecture, and construction. The students also learned to **cooperate, debate and express their ideas** amongst one another, designing proposals for the development of their territory.

{{< framework/video url="https://media.iri.centrepompidou.fr/video/unej/expo%20UNEJ/VILLAGE%20AQUATIQUE.mp4">}}


You can find more on the history of the project, as well as information and exemplars of the student projects on [this site  dedicated to the project](https://unej.tac93.fr/).

Drawing on experience acquired during the multiple workshops conducted within various establishments, the IRI and its partners have formalized elements of the **pedagogical method** to allow others to extend the project to other contexts of city planning and imagination. This proposal is aimed at communities, developers, social real estate companies, and other places who wish to include residents in their decision-making processes through a new immersive process possible at this dawn of city digitalization.

### Project gallery

{{< gallery Match="gallery/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview=true loadJQuery=true >}}
