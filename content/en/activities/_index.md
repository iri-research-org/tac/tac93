---
title: "Activities"
description: "A selection of activities connected to the programme TAC"
date: 2018-02-10T11:52:18+07:00

header_transparent: true

hero:
  headings:
    heading: "Activités"
    sub_heading: "A selection of activities connected to the programme TAC"
  background:
    background_image: "images/photos/horizontal-front-image.png"
    background_image_blend_mode: "overlay"
    background_gradient: true
---

