---
title: "Comité Eco"
description: "Unite community members and economic actors, support ecological and social initiatives through a new local currency."
date: 2024-11-20
weight: 3
portfolio: ["research"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Comité Eco"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Unite community members and economic actors, support ecological and social initiatives through a new local currency."

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/eco.png"
icon: ""

hero:
  headings:
    heading: "Comité Eco"
    sub_heading: "Unite community members and economic actors, support ecological and social initiatives through a new local currency."
  background:
    background_image: "images/photos/activities/eco_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2022-
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Plaine commune"
    link: https://plainecommune.fr/
  - name: "Odyssée"
    link: http://odyssee.co/fr/identity.html
  - name: "La Régie"
    link: https://www.facebook.com/RegiedeQuartiersdeSaintDenis/?locale=fr_FR
  - name: "PHARE"
    link: http://www.lephares.coop/
  - name: "Mains d’ouvres"
    link: https://www.mainsdoeuvres.org/
  text: "The implementation of a new local currency supports new connections between community inhabitants, associations, business, and societies; valorising ecological action and social community building. The project is driven by the urban transformation think tank, Odysée, and supported by the Institut de recherche et d’Innovation."
  link_label: Site web
  link_url: "https://www.comite-eco.fr/comite-eco/"
  fundings: 
  - name: "Société de livraison des ouvrages olympiques"
    link: https://www.ouvrages-olympiques.fr/
  - name: "Fondation de france"
    link: https://www.fondationdefrance.org/fr
  - name: "Fondation Macif"
    link: https://www.fondation-macif.org/
---

Historically, works by Ars Industrialis and the IRI focus on the question of a contributory currency and ways of counting – from an accounting point of view. This work remained very theoretical for many years. In 2022, an opportunity for practice emerged following a call from SOLIDEO named “Smart Citizen," which asked how we might encourage residents’ behavior to fuel the **ecological transition**. Created in 2023 to respond to this call, the Committee ECO is an association in the Plaine Communet territory. The association's mission is to develop a local currency, **ECO**, which would bolster the capacities of local actors in the territory. It brings together the IRI and Odyssée, an urban planning agency. The project aims to help residents **regain control over their food, their living environment, their mobility, their consumption, and the biodiversity** of their communities through the use of this currency and the applications that accompany it. The Comittee ECO aims to provide the tools that can supports people's commitment to ecological tranformation while also fascilitating meetings and exchanges to promote social ties in the region.

![eco app](/images/photos/activities/eco_text1.png)

The project has specific objectives : **currency circulation**, notably during the 2024 Olympic Games amongst local merchants, promoting “**life trajectories**”: the steps to take in order to gain and use **ECO**. In addition, using ECO will imply the constitution of an economic reserve (3% of the exchanges  of ECO) which the collective will be able to use – for which the details are still being finalised. 

In order to define which actions would potentially fall within the framework of what ECO currency wishes to promote for the territory, a **scientific committee** will be set up, uniting researchers, residents, and elected officials to reflect and decide which actions would carry potential impact for the territory in line with sustainability objectives. This requires meticulous, diligent, and careful reflections. This research work is already underway, particularly in terms of the economic questions, such as : What generates motivation ? To what extent could this currency generate the motivation to take action ? How can we ensure that all these acts motivate funding ? The project is a practical attempt to **take an interest in the territory's economy**, but requires ongoing and considerable theoretical analyzes and critiques.
