---
title: "Seminars on Currencies"
description: "A space of exchange, research, and debate on the contributory economy and the role of local currencies"
date: 2024-11-20
weight: 6
portfolio: ["research"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Seminars on Currency"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "A space of exchange, research, and debate on the contributory economy and the role of local currencies"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/seminaire-monnaie.png"
icon: ""

hero:
  headings:
    heading: "Seminars on Currency"
    sub_heading: "A space of exchange, research, and debate on the contributory economy and the role of local currencies"
  background:
    background_image: ""
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2016-
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Université de Bordeaux"
    link: https://www.u-bordeaux.fr/
  text: "Supported by the Caisse des dépôts (Deposits and Consignments Fund) and the Fondation de France, the annual Seminars on Currency hosts sessions with the leaders of projects demonstrating economic innovation, seeking to extend or enhance the principles of the contributory economy, including insights into local currencies, calculative models, cooperative models, economic propositions, and new areas of investment."
  link_label: Enregistrement du séminaire monnaie 2024
  link_url: "https://iri-ressources.org/collections/season-72.html"
  fundings: 
  - name: "Caisse des dépôts"
    Link: https://www.caissedesdepots.fr/
  - name: "Fondation de France"
    link: https://www.fondationdefrance.org/fr/
---

Description to come.