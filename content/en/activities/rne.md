---
title: "Raisonnons nos écrans (Reason with our screens)"
description: "Parents, researchers, and professionals unite, building capacities and practices to reason with and manage their screens"
period: 2022-2025
date: 2024-11-20
weight: 4
portfolio: ["research"]
partenaires: ["Institut de recherche et d’innovation", "Ville de Saint Denis", "Mildeca"]
axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Raisonnons nos écrans (Reason with our screens)"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Parents, researchers, and professionals unite, building capacities and practices to reason with and manage their screens"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/rne.jpg"
icon: ""

hero:
  headings:
    heading: "Raisonnons nos écrans (Reason with our screens)"
    sub_heading: "Parents, researchers, and professionals unite, building capacities and practices to reason with and manage their screens"
  background:
    background_image: "images/photos/blur/rne_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Ville de Saint Denis"
      link: https://ville-saint-denis.fr/
  text: "Six training programmes with 9 sessions each, proposed by the City of Saint-Denis and followed by 60 professional and 30 parents since 2021. The project seeks to work collectively to invent strategies to respond to the screen overexposure observed among small children."
  link_label: 
  link_url: 
  fundings:
    - name: "La Mildeca"
      link: https://www.drogues.gouv.fr/
---

Since 2022 and for three years, the Contributory Clinic has led a training project titled “Raisonnons nos écrans" (Let's reason with our screens). The group brings parents and health professionals together as part of a **partnership with the Mission interministérielle de lutte contre les drogues et les conduites addictives et la Mairie de Saint-Denis (Interministerial Agency Mission to fight against drugs and addictive behavior) as well as the Saint-Denis Town Hall**. These training sessions take place in Saint-Denis and alternate between neighborhood centers, media libraries, and social centers.

![RNE session](/images/photos/activities/rne_text1.jpg)

These training courses last 9 sessions, reaching around a hundred people in total. They take place every two weeks over a half- day, and are inspired by the Contributory Clinic's method. In these workshops, researchers, parents, and health professionals reflect on the problem of **overexposure to screens**, seeking to create new knowledge practices to address this situation, which has become part of modern society. 

The workshops implement:
- The individual and collective analysis of the participants' digital practices, based on the principle that everyone, whatever their status, is involved in the problem of overexposure to screens -- and that everyone brings something to contribute to **collective reflection**.
- Theoretical contributions on the functioning of the brain, psychological vulnerabilities, the needs of **child development** at young ages, the functioning of digital applications and the attention economy, the group's therapeutic potential, questions of addiction, parent-child relationships, etc.
- A collective reflection and exchange of **digital detoxification practices**, including the exchange of tips to change one's relationship with screens.
- The construction of **new professional practices** and actions to be carried out. The workshops include discussions about projects to initiate, from parent cafés to interventions in schools and games to raise awareness in workplace. 