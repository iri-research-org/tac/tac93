---
title: "ContribAlim"
description: "Contributive creation of recipes for a collective kitchen"
period: 2022-2024
date: 2024-11-20
weight: 5
portfolio: ["research"]
axe: economie-de-la-contribution

header_transparent: false

# menu:
#   main:
#     name: "ContribAlim"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Contributive creation of recipes for a collective kitchen"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/contribalim.png"
icon: ""

hero:
  headings:
    heading: "ContrbAlim"
    sub_heading: "Contributive creation of recipes for a collective kitchen"
  background:
    background_image: "images/photos/activities/contribalim_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  period: 2022-2024
  client_label:
  client_content: 
  partners:
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Open Food Fact"
    link: https://world.openfoodfacts.org/
  - name: "Mosaic"
    link: https://mosaic.mnhn.fr/un-nouvel-outil/
  - name: "Université Sorbonne Paris Nord"
    link: https://www.univ-spn.fr/
  - name: "The neighborhood of Saint Denis"
    link: https://ville-saint-denis.fr/
  - name: "Appui"
    link: https://appuii.wordpress.com/
  - name: "Ouishare"
    link: 
  - name: "Crisalim"
    link: https://www.crisalim.co/
  text: "ContribAlim is a participatory science and contributory research setup for the development of food knowledge, deployed in Sein Saint-Denis and funded by the ADEME."
  link_label: Application web
  link_url: "https://contribalim.mnhn.fr/"
  fundings: 
  - name: "ADEME"
    link: https://www.ademe.fr/en/frontpage/
---


How can we develop food standards and scientific knowledge to support a process of capacitation in a region exposed to multiple forms of food insecurity ? Better food sourcing, better cooking, the sharing of meals, cultivation of flavors, fight against food pathologies... how do we build this momentum and knowledge ? Under the direction of L'Institut de Recherche et d'Innovation (IRI), and thanks to funding from ADEME as part of the CO3 program, the ContribAlim project brought together research teams from 2022 to 2024 (Mosaic, LEPS, EREN), organizations from the neighborhood of Saint Denis, (APPUI, Crisalim), the city's health service and the company OpenFoodFacts to co-design a contributory platform for recipes connected to Nutriscore.
![screenshot du site de web](/images/photos/activities/contribalim_text1.png)