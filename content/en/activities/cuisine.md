---
title: "Contributive kitchen"
description: "Cook large quantities of food together"
date: 2024-11-20
weight: 5
portfolio: ["research"]
partenaires: ["Institut de recherche et d’innovation", "Taf et Maffé", "La Petite Casa"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Contributive kitchen"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Cook large quantities of food together"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/cuisinecontributive.png"
icon: ""

hero:
  headings:
    heading: "Contributive kitchen"
    sub_heading: "Cook large quantities of food together"
    period: 2022-2024
  background:
    background_image: "images/photos/activities/cuisinecontributive_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content:
  period: 2022-2024
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Taf et Maffé"
    link: https://plainecommune.fr/
  - name: "La Petite Casa"
    link: http://odyssee.co/fr/identity.html
  text: "This integrated program is part of the ECO Project, and seeks to forge ethical and ecological practices that can capacitate people in their careers, supporting the valorization of ecological knowledge practices in new economic activities."
  link_label: 
  link_url: ""
  fundings: 
---


Contributory cuisine responds to the growing call for **food democracy**, which aims to create concrete conditions for residents to choose their food in an informed manner, with respect to their agency, dignity, history, and culture. In this sense, the project works primarily with people who experience difficulties related to food (small budgets, recourse to food aid, eating pathologies, isolation, difficulties self-organising, etc.); using a non-stigmatizing approach based on their experiences and desires. The project seeks to inspire participants to gradually take charge of the different stages of food preparation and food knowledge. This project is inspired by a practices developed in Quebec , **collective kitchens**, focusing on the **regrouping of small groups of inhabitants** who pool their time, money, and knowledge practices in order to cook together in an economic, healthy, and tasty fashion. Participants then bring these plates and practices home with them. In this spirit of contribution and collectivity, the project ContribAlim unites each month a group of 8 to 10 people, composed of young professionals and students to cook **meals that everyone can bring back home with them**. 

The atelier runs along 4 phases :

1. Planning : Collective discussion and definition of the recipes to cook, and the number of portions that each participant would like of the different plates; establishment of the session's budget
2. Purchases
3. Cooking
4. Evaluation of the learning program and the plates

To enable these different phases to be carried out in a constructive and collaborative manner, each session is broken down into two half-days which take place within the Taf et Maffé restaurant.

![cuisine1](/images/photos/activities/cuisinecontributive_text1.png)

The project has two objectives : 

- To offer a **capacitating response to the problem of food insecurity** : this involves promoting a multilayered approach to tackle the multiple issues linked to this situation. The Contributive Kitchen's approach lessens the economic burden by pooling purchases to reduce cost; strives to be health conscious by working on the nutritional balance of dishes; and considers the social aspect of cooking with care, creating links between participants who would not otherwise meet. Finally, the project seeks to engage cultural practices as well, **making space for the food culture of the participants** who collaborate on the recipes which will be cooked.
- Establish a “laboratory” where new economic, cultural or social activities in the region can be developed based on the knowledge developed collectively. Such activities include catering, a neighborhood canteen, an artisanal cannery, etc.