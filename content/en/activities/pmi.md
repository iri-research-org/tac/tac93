---
title: "PMI Pierre Semard Ateliers"
description: "A contributive research effort to produce new knowledge schemas on the overexposure to screens"
date: 2024-11-20
weight: 2
portfolio: ["research"]
axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Ateliers at the PMI Pierre Semard"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "A contributive research effort to produce new knowledge schemas on the overexposure to screens"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/pmi.jpeg"
icon: ""

hero:
  headings:
    heading: "Ateliers at the PMI Pierre Semard"
    sub_heading: "A contributive research effort to produce new knowledge schemas on the overexposure to screens"
  background:
    background_image: "images/photos/activities/pmi_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: "2017-"
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "PMl Pierre Sémard"
      link: 
    - name: "CNRS"
      link: https://republique-des-savoirs.fr/en/home/
    - name: "Ville de Saint Denis"
      link: https://ville-saint-denis.fr/
  text: "Since 2017, parents have convened every week with researchers and professionals at the PMI Pierre Sémard for workshops on the practical and theoretical collective capacitation on questions concerning the overexposure of screens to children. These workshops seek to draw on different strategies, including games, gardening, and writing."
  link_label: Wiki de la Clinique Contributive
  link_url: "https://cloud.svc.iri-research.org/sites/clinique_contributive/"
  fundings: 
    - name: "Fondation de France"
      link: https://www.fondationdefrance.org/fr/
---

Today, the omnipresence of screens and its accompanying overexposure is increasingly recognized as a public health issue ([as evidenced by the commission set up by President Macron at the beginning of 2024]( https://www.info.gouv.fr/actualite/pas-decran-avant-trois-ans#:~:text=La%20commission%20écrans%20a%20rendu,adultes%20vers%20de%20bonnes%20pratiques.)). In this context, the IRI created the Contributory Clinic in November of 2017, **an experimental program** implemented with the team of health professionals from **Maternal and Child Protection** (**PMI**) Pierre Sémard, located in Saint-Denis. Every two weeks, researchers from various disciplines come together with health professionals and parents to **collectively invent new knowledge practices for digital technologies**, which are becoming increasingly widespread in children's education. The project aims to respond in an innovative way to these emerging modern problems of technology.

Several projects are carried out with the PMI team.

## Booklet 

In 2023, we created an **s booklet to raise awareness on screens**, which was finalized with the Saint-Denis town hall, groups of health professionals, parents, and researchers. The project sought to deconstruct pervasive assumptions such as: “the screen helps the child calm down so that they stop crying”. Following the response to these different assumptions, the booklet aimed to offer scientifically supported explanations, including regionally specific **alternative activities to screens** found in Seine-Saint-Denis. You may find the booklet [here](https://www.calameo.com/ville-saint-denis/read/0003435242149a8b77e0a?page=9).

![Screen booklet](/images/photos/activities/pmi_text1.png)

## Gardening atelier 

In spring 2024, we started a contributory gardening workshop, cultivating the garden on the PMI premises, as well as discussing and discovering theoretical knowledge in botany. The workshop was founded to **experiment different meaning of the word cultivation**. Culture, in its broadest sense, requires time, investment, and practices. Cultivating a garden, imagining and inventing a space for growth, and discovering the species of plants as well as effects of wind and sun, are all processes connected to self-cultivation. We believe that **you transform yourself by transforming what you have in your hands**. 

At the start of the 2024 school year, we continued to plant new species, observe the flowers that grew spontaneously in the garden, and discovered botanical drawing.

![garden](/images/photos/activities/pmi_text2.jpeg)

## Writing

In the years 2024-25, the PMI group will begin a long-term writing project with the aim of documenting the knowledge created since the start of the project. We will work on existing **games** made to respond to the issue of screens in order to write reviews and comments, which will be the starting point of this collective writing work. This aspect of the project will both challenge us and build our identity as a writing group -- perhaps allowing us to expand on our theory and practice through this process.


## Theoretical seminar

In parallel with all these activities, a theoretical seminar takes place every two months, where researchers from different disciplines are invited to deepen the theoretical aspects linked to the overexposure to screens. These sessions have allowed us to cover topics such as **language development**, the **relationship between parent and child**, **addiction**, **disruption**, **autism**, **the attention economy**, the **human relationship with images**, and questions of the body and **psychomotor skills**.

![writing](/images/photos/activities/pmi_text3.jpg)
