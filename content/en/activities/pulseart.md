---
title: "Pulse-Art"
description: "Experiment and evaluate the impact of cultural awareness and expression in education"
date: 2024-11-20
weight: 5
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Pulse-Art"
#     weight: 6
#     parent: "activities"
#     params:
#       description: "Experiment and evaluate the impact of cultural awareness and expression in education"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/pulseart.jpg"
icon: ""

images: []

hero:
  headings:
    heading: "Pulse-Art"
    sub_heading: "Experiment and evaluate the impact of cultural awareness and expression in education"
  background:
    background_image: "images/photos/pulseart/pulse-art-01-1024.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2025-
  text: "This project seeks to analyse 7 case studies of cultural awareness and expression, exploring their role in education and capacitation (in and out of the classroom)"
  link_label: 
  link_url: ""
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Science for Language"
      link: https://scienceforchange.eu/
    - name: "Barcelona lnstitute for Health"
      link: https://www.isglobal.org/
    - name: "WAAG Future Lab"
      link: https://waag.org/
    - name: "Anatolia College"
      link: https://anatolia.edu.gr/en/about/institutional-description
    - name: "Foundation for Research and Technology"
      link: https://www.forth.gr/en/home/
    - name: "Museo Nacional de Ciencias Naturales"
      link: https://www.mncn.csic.es/en/quienes_somos/presentacion
    - name: "University of Malta"
      link: https://www.um.edu.mt/
    - name: "Euro-mediterranean Economists Association"
      link: https://euromed-economists.org/
    - name: "Daugavpils University"
      link: https://du.lv/
  fundings: 
    - name: "Union européenne"
      link: https://erasmus-plus.ec.europa.eu/

---

PULSE-ART aims to understand and promote the **development of cultural knowledge and skills for the education and capacitation of European youth** (Cultural Awareness and Expression, CAE). The project instills lifelong learning perspectives by identifying educational gaps and overcoming learning obstacles in order to foster opportunities to increase the capacities of the educational system, in order to support learners in their acquisition of knowledge practices. The project involves various educational institutions, arts and cultural heritage organizations, as well as innovation centers, working in collaborative partnership to produce pedagogical practices, project proofs, and inform policy.

The 7 targeted case projects:

1. Game jams (Université de Malte, MA)
2. Video games (IRI, FR)
3. Stage arts (Waag, NL)
4. Scientific illustration (Musée national des sciences naturelles, ES)
5. Dance (Université de Daugavpils, LV)
6. Digital arts (Fondation pour la recherche et la technologie Hellas, GR)
7. Music (Association Euro Méditerranéenne des Economistes, MR)

![kick off workshop](/images/photos/activities/pulseart_text1.jpg)
