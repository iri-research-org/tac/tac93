---
title: "En veille Association"
description: "Parents stand together against the overexposure to screens"
date: 2024-11-20
weight: 1
portfolio: ["research"]
header_transparent: true

# menu:
#   main:
#     name: "En veille Association"
#     weight: 5
#     parent: "activities"
#     params:
#       description: "Parents stand together against the overexposure to screens"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/parentsa.jpg"
icon: ""
axe: parentalite-soin-numerique

hero:
  headings:
    heading: "En veille Association"
    sub_heading: "Parents stand together against the overexposure to screens"
  background:
    background_image: "images/photos/activities/parentsa_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2024-
  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
  text: "An association of parents supported by the IRI and CAF93 works to share insights and fascilitate discussions for groups, professionals, and parents confronted with the impacts of screens."
  link_label:
  link_url:
  fundngs:
    - name: "CAF93"
      link: https://www.caf.fr/allocataires/caf-de-la-seine-saint-denis
---

Over the years, the Contributory Clinic and its training program “Raisonnons nos écrans" (“Let’s reason with our screens”) have introduced us to a number of motivated parents. “**En Veille. Let’s reconnect with each other.**” was started by a group of these parents who took the initiative to unite in the spring of 2024. The group's founding parents mirror the experience of many other parents; they have sometimes overexposed their children to screens, they have become aware of overexposure's dangers, and they have found new practices to counteract these problems. These parents have developed **know-how and practices for encouraging their children away from screens**, offering alternative activities and reasserting their agency as parents. Their testimonies and their ongoing findings offer both encouragement to parents in difficulty as well as insights for professionals who often express doubts about how to talk about the problem of overexposure without making parents feel guilty. 

![En Veille meeting](/images/photos/activities/parentsa_text1.jpeg)

## The three main objectives of the association

- **Raise awareness amongst other families**, professionals, and residents about the overexposure to screens and its dangers for children.
- Propose alternative activities to screens in the Seine-Saint-Denis region.
- Perpetuate the link with the world of research; continue to train, learn, and organize seminars on the subject.

## The financial question

The IRI is also reflecting on the question of **financial conditions for parental participation**. This reflection takes place through the contributory economy model with a contributory income to which parents would be entitled a priori, renewable on condition of valorizing the knowledge produced and acquired within the framework of intermittent contributory employment. We are presently working on this model, with several questions arising in this process: how to formalize and transmit knowledge? How to manage the financial modalities of intervention and remuneration in the group? What potential funds could support the actions of parents? This reflection is ongoing.

With the support of the IRI and CAF93.


