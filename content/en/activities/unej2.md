---
title: "Digital Urbanity in the Games #2"
description: "Knowledge, creativity, and expression in urban deliberation projects"
date: 2024-11-20
weight: 5
portfolio: ["research"]
partenaires: ["L’institut de recherche et d’innovation", "Cabinet of Architecture and Urbanism O’zone", "ICI !", "Construction Initiatives by Îlo-Dionysiennes (association of architects)", "3 Hit Combo Association, leader of the RennesCraft project", "Regional education authority of the Academy of Créteil", "le CDOS 93 (Departmental Olympic and Sport Committee of Seine-Saint-Denis)"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Digital Urbanity in Game #2"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Knowledge, creativity, and expression in urban deliberation projects"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/unej2.JPG"
icon: ""

hero:
  headings:
    heading: "Digital Urbanity in Game #2"
    sub_heading: "Knowledge, creativity, and expression in urban deliberation projects"
  background:
    background_image: "images/photos/activities/unej2_background.JPG"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2025-
  text: "The dynamic created with educational establishments enhances the framework of urban planning, through the collaboration with architects, artists, fabLabs and collectives."
  link_label: 
  link_url: ""

---

Building on the experience of the UNEJ project, the IRI has relaunched dynamics from the Digital Urbanities in Games (UNEJ) project in a new phase.

This second phase aims to raise awareness among young residents within a given region on the making of the city by involving them in **urban consultation**. This involves the development of theoretical knowledge, know-how and soft skills linked to aspects of town planning with regard to the environment, gender, social integration, and intergenerational identities; all with attention and collaboration through responsible digital technologies. The project seeks to engage residents to take action in their communities. The project also extends its efforts within a context of increasingly connected cities, which surveil and collect data on their inhabitants without giving them the capacities to take action in their neighborhoods' design, or even to understand how their city is made. This work on **territorial planning**, includes questions regarding resources, architecture, and urban planning.

Through a digital program which sets up the game **Minetest** (free and open equivalent of Minecrcaft, which is among the most played games in the world), the project seeks to mobilize an audience of young residents, who may be more easily engaged than when using traditional consultation tools. This system familiarises young people with digital technology, but even more with this type of approachable, "sandbox" game. In engaging the students, groups can build and reimagine their world, to close the gap between young people and civil society. Here the students' citizenship in their communities is reaffirmed, and their right to the city put to practice as they work to create their city together.

*Method* : 
Use of Minetest in connection with geographical data from the IGN (National Institute of Geographic and Forestry Information) as a city modeling tool. This program offers a simplified digital model of the territory. 
- Setting up group work arrangements through **modeling workshops**, urban walks, etc. for a project-based approach. Each group must subsequently debate and present its project, which allows the discussion, debate, and collaborations of ideas within the project.  
- Experimentation and principles of trial and error (we try, we can be wrong, we start again, we discover...)
- **Valorisation of the territory**, rereading of atlases, reinforcement of the **urban imagination**, rejuvenation of how young residents look at their places of community and life, renewal of their imaginations (notably through mangas ...)
