---
title: "RegenerAction"
description: "Identify the ‘Urban Community Builder’ as a professional figure and occupational role in society"
date: 2024-11-20
weight: 6
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "RegenerAction"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Identify the ‘Urban Community Builder’ as a professional figure and occupational role in society"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/regeneraction.jpeg"
icon: ""

hero:
  headings:
    heading: "RegénérAction"
    sub_heading: "Identify the ‘Urban Community Builder’ as a professional figure and occupational role in society"
  background:
    background_image: "images/photos/activities/regeneraction_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2023-2025
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Urbani Separe"
    link: https://www.urbanisepare.org/
  - name: "Matera Hub"
    link: https://www.materahub.com/
  - name: "Innohub"
    link: https://www.innohub.uk/
  - name: "University of Évora"
    link: https://www.uevora.pt/en
  text: ""
  link_label: Website
  link_url: "https://regeneractionproject.eu/"
  fundings: 
  - name: "European Union"
    link: https://erasmus-plus.ec.europa.eu/
  - name: "Surviving Digital"
    link_url: "https://survivingdigital.eu/"
---

In the **Design and Technologies for Urban Contributory Research** axis, this project seeks promotes exchanges between European partners to define, document, and experiment notions around the figure of the “Urban Community Builder” through capacity building workshops focused on questions around recovery, short circuits, and how the circular economy can serve as a new urbanity. 