---
title: "Surviving Digital"
description: "Cultivate awareness and practices of care in the information age"
period: 2022-2024
date: 2024-11-20
weight: 5
portfolio: ["research"]

axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Surviving Digital"
#     weight: 1
#     parent: "activities"
#     params:
#     description: "Cultivate awareness and practices of care in the information age"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/surviving-digital.jpeg"
icon: ""

hero:
  headings:
    heading: "Surviving Digital"
    sub_heading: "Cultivate awareness and practices of care in the information age"
  background:
    background_image: "images/photos/activities/surviving_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 

  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Le Laba"
      link: https://lelaba.eu/laba/
    - name: "Momentum"
      link: https://momentumconsulting.ie/
    - name: "Iasis"
      link: https://www.iasismed.eu/
    - name: "Materahub"
      link: https://www.materahub.com/
    - name: "La ville de Saint Denis"
      link: https://ville-saint-denis.fr/
    - name: "European E-learning Institute"
      link: https://www.euei.dk/
  text: ""
  link_label: Site du projet
  link_url: "https://survivingdigital.eu/"
  fundings: 
    - name: "Union européenne"
      link: https://erasmus-plus.ec.europa.eu/

---

Piloted by the LABA agency, the Surviving Digital project seeks to formalize experiences and research carried out in Seine-Saint-Denis since 2019, in order to share it effectively with a larger public and provide insights into how to conduct contributory workshops on these subjects. Aimed at parents and professionals affected by overexposure and addiction to screens, the project led to 4 major results:  

- A study presenting the issues of screen addiction.
- A method for creating support groups to combat screen addiction, based on the experiences of partners. The IRI led the drafting of a [methodological guide](https://cloud.svc.iri-research.org/sites/clinique_contributive/sub/guide%20methodologique#page-toc-8) with insights into the implementation of field actions.  
- Two certified training courses dedicated to professionals supporting young parents and families. 
- A toolbox made up of 10 educational videos for group discussions, as well as training and informational tools for adult audiences and trainers.

The Surviving Digital project is made up of specialists in health, digital technology and adult education in Ireland, Italy, Greece and France.

![Atelier in Athens](/images/photos/activities/surviving_text1.jpeg)

In April of 2024, we were delighted to participate in a meeting in Athens with two of the mothers from the En Veille association, Nadia (who was present during the RNE training and was very motivated to continue the work) and Hakima (president of En Veille who has followed the project since the beginning). The mothers presented the work of the Contributory Clinic, discussing the method used and several of the major concepts that impacted them. They also led a workshop based on the model of the RNE training. The resulting exchanges between countries were very rich; we discussed at length the issues of boredom and frustration in both English and French, and we are very proud of this moment!

![Athens presentation](/images/photos/activities/surviving_text2.jpeg)
