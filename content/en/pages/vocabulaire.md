---
title: 'Vocabulaire'
weight: 3
date: 2018-12-06T09:29:16+10:00
background: ''
align: right
url: "vocabulaire"
buttonLink: 'vocabulaire'
image: "images/photos/content/content-6.webp"

vocabulaire:
    - title: Anthropocene
      definition: 'This term was proposed by the Nobel Laureate and chemist Paul Crutzen to describe the geological era that commenced when human activity began to have a significant global impact on Earth’s ecosystem and the future of the planet. The opening of this new era occurred in the late eighteenth century with the industrial revolution and it now brings the very possibility of life continuing on Earth into question. The Anthropocene can be described as an ‘Entropocene’ insofar as this period corresponds to a massive increase in the rate of entropy at the physical level (dissipation of energy), the biological level (destruction of biodiversity) and the psycho-social level (destruction of cultural and social diversity).'
      nr: One

    - title: Anti-entropy
      definition: 'Anti-entropy is a tendency towards structuration, diversification and the production of novelty. It is explained by the fact that the organization of living things locally and temporarily opposes the law of the inevitable increase in entropy. Anti-entropy is in this respect the process that characterizes life insofar as it struggles against the dissipation of energy and the disorganization that is its result, and insofar as it produces, among other things, new functions and new organs. The notion has been generalized to describe everything that tends to create difference, choice or novelty in a system, everything in the development of a system that tends to self-conservation and/or transformation towards improvement.'
      nr: Two

    - title: Disruption
      definition: 'Disruption refers to the upheaval of social organizations and institutions (from the family to government via businesses, languages, law, economic regulation, taxation, etc.) through the highly rapid development of new technologies (radical and permanent innovation). Disruption results from the fact that the evolution of the technical system is occurring far quicker than the evolution of social systems. This disadjustment or disconnection between the evolution of the technical system and the evolution of social systems is not new (Bertrand Gille describes it as typical of the industrial revolution). Today, however, these technical transformations occur so quickly that they leave the political and social realms behind, as well as public power in general, so that no new viable model of long-term social and economic development can emerge. Regulation, legislation and knowledge always arrive too late in their attempts to appropriate the new: the resulting constant expansion of legal vacuums and theoretical vacuums is without historical precedent.'
      nr: Four

---


