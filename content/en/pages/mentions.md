---
title: 'Legal Mentions'
date: 2024-11-29
menu:
  bottom:
---

Date de la dernière mise à jour : 29/11/2024

## Legal Mentions

The site “tac93.fr” and elements composing it (all texts, photographs, videos and sound documents) are the property of IRI, or from a third party from whom the rights of use have been obtained. They cannot therefore be used without prior authorization under the provisions of the Intellectual Property Code.

Consequently, any representation or reproduction in whole or in part, as well as any use, adaptation, transformation, or arrangement of all or any part of the works, distinctive signs, or databases, are protected by the intellectual property rights of the IRI. Any use or incorporation of this data without the IRI's consent is illegal, and constitutes an infringement punishable under criminal law according to article L 122-4 of the Intellectual Property Code.

## Technical Information

The IRI is the owner and host of the “tac93.fr” website.

This site is created with [Hugo](https://gohugo.io/).

## Editorial information


### Director of publications

Olivier Landau

### Editor

Vincent Puig

### Project manager

Guillaume Pellerin

### Website designers 

Robert Austin, Guillaume Pellerin

### Edition

Elvira Hojberg

## Coordinators

IRI - Institut de Recherche et Innovation
4 rue Aubry le Boucher 75004 Paris, France

SIRET : 50892348900019

RNA : W751187560

Téléphone : +33 183 87 63 25

### Host

Centre Pompidou - Place Georges-Pompidou, 75004 Paris

Téléphone : 01 44 78 12 33
