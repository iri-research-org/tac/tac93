---
title: "The TAC Programme"
date: 2018-11-28T15:15:34+10:00
url: "programme"

description: "A territorial experiment in contributory research which seeks to develop an economy of contribution."
image: "images/photos/programme.jpeg"

tables:
  ressources:
    name: "Resources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: Programme Général
        authors: Bernard Stiegler
        date: 2017
        link: "/pdfs/programme_general.pdf"
      - title: "Contributory economy and creation of common goods"
        authors: Clément Morlat, Théo Sentis, Olivier Landau, Anne Kunvari, Vincent Puig
        date: 2021
        link: https://anis-catalyst.org/imaginaire-communs/economie-de-la-contribution-et-gestion-des-biens-communs/#:~:text=Le%20commun%20sur%20lequel%20s,l'%C3%A9conomie%20de%20march%C3%A9%20globalis%C3%A9e.
      - title: "Bifurcate: there is no alternative"
        authors: Internation ed. Stiegler
        date: 2019
        link: https://www.openhumanitiespress.org/books/titles/bifurcate/
      - title: "Work Marathon"
        authors: Serpentine Gallery and the InstItute of Research and InnovatIon
        date: 2018
        link: https://iri-ressources.org/collections/collection-47.html
      - title: "ENMI 2019 : International, internation, nations, transitions: penser les localités dans la mondialisation. Pour fournir des éléments de réponse à António Guterres et Greta Thunberg (ENMI 2019: International, internation, nations, transitions: thinking about localities in globalization. To form a response to António Guterres and Greta Thunberg)"
        authors: L'Institut de recherche et d'innovation
        date: 2019
        link: https://enmi-conf.org/wp/enmi19/

---

<section id='ambition'>
<h2>Bifurquer pour un avenir soutenable</h2>
 
The age of the Anthropocene is characterized by the **rupture of all major natural balances** (global warming, erosion of biodiversity, etc.) driven by the impact of human activities. The philosophical heritage of Bernard Stiegler leads us to think that what is fundamentally at stake in the climate crisis is **a particular relationship between human beings and technology**. Transport, agriculture, construction...: in two centuries, each field of human activity has been considerably transformed by the invention and mobilization of new technical tools. Initially perceived as a form of progress, this transformation has since revealed its more toxic aspects: humanity is also suffering the consequences of a technological evolution of which it has partly lost control and mastery. 

Read in this light, **bifurcating towards a more sustainable future requires another mode of development, based on the reappropriation of the technological environment which surrounds us**, in order to limit its toxicity. We argue that this reappropriation requires the **development of new knowledge practices**, which can strengthen the social fabric, in a dynamic where each individual contributes with their own particular knowledge to create new technological practices, while the practices cultivated, in turn, allows everyone to develop their own unique singularities. Unlike many training programs that focus on the acquisition of pre-existing skills, our **collective capacitation approaches** seek to nuture individuals and collectives that can act on a rapidly changing world. In order for these processes to be solvent, they must be integrated into a new economic model, what we call a contributory economy. 
Therefore, TAC takes steps towards building this contributory economy and implement capacitation approaches together with residents, researchers, and professionals who constitute the TAC program.</p>


</section>
 
<section id='methode'>
<h2>Prepare and experience a contributive economy</h2>

The contributory economy aims to ensure the robustness of capacitation processes, and ensure that it is economically viable for the inhabitants of a territory to participate by granting them a **conditional contributory income**. The contributory economy creates, in this way, the conditions for a systemic transformation by which residents will be encouraged to collectively develop knowledge practices. Following this logic, residents may put these knowledge practices at the service of their territory, thus creating “**contributory learning territories**“. This transformation of the local economy into a knowledge-based care economy will allow the emergence of new contributory jobs.  As in the system of intermittent workers in the entertainment industry, a resident will be able to renew their contributory income by occupying a contributory job for a fixed period.

To be truly demonstrative, such an experiment requires preliminary research in the form of fieldwork. It is therefore necessary to carry out a **permanent investigation** to identify the territorial dynamics which take part in a contributory economy. It is then a matter of uniting actors through **capacitation workshops**. These workshops combine both research and field experimentation of new practices, opening paths to new knowledge. They rely on the method of **contributory research**, which consists of getting residents, professionals, and university researchers to work together in projects designed to offer equal opportunities to each participant, contributing to the research dynamic and capacitating participants to become both researchers and actors of their own futures.

The objective of the TAC program is to continue the development and experiment this contributory economy in Seine-Saint-Denis.
</section>

<section id='origine'>
<h2>Where does TAC come from ?</h2>
 
This program pursues the reflections of the IRI and Ars Industrialis on the economy of contribution, taking shape in 2016 through **a dialogue between Bernard Stiegler, the IRI team, and Patrick Braouezec**, the then president of the EPT Plaine Commune, as well as elected officials of the territory. Since the early 2000s and based on the philosophical work of Bernard Stiegler, the IRI and Ars Industrialis have been working on multiple issues arising from the societal shift to digital technologies. From 2010, discussions focused on the economic field, particularly around the creation of a new economic and industrial model, based on the transition from a consumption economy to a contribution economy. It is on this historical and theoretical basis that the TAC program was designed, carried out today by the IRI team throughout Seine-Saint-Denis.

- 2016 : Launch of the TAC program, the initiative of Bernard Stiegler and Patrick Braouezec, with the support of Caisse des Dépôts, the Fondation de France, and a group of industries.
- 2017 : Launch of the [Clinique Contributive](/activities/pmi/)
- 2019 : Beginning of the project [Urbanité Numérique en Jeux](/activities/unej) with the support of l’Académie de Créteil and the department of Seine-Saint-Denis
- 2020 : The IRI is selected by the Fondation de France to take part in the network [Acteurs clés de changement (Key actors of change)](https://www.fondationdefrance.org/fr/cat-inventer-demain/le-programme-inventer-demain-vu-par-celles-et-ceux-qui-le-font)
- 2021 : Opening of the workshop [Cuisine contributive](/activities/cuisine/) and the beginning of the project ContribAlim, supported by l’ADEME
- 2022: A group of actors led by the Institut de Recherche et d'Innovation, the agency Odyssée, and Plaine Commune was selected by the Solidéo to develop a system to support the ecological transition using a local currency.
- 2024: ÉCO currency is introduced in Saint-Denis, l’ile-St-Denis, et St Ouen
- 2024: Creation of the association En veille following activities carried out in the parenting, care, and digital axis. 
- 2024: Exhibition of the results of the UNEJ project at the Serre Wangari and launch of a new three-year phase with the support of Fondation de France, Caisse des Dépôts, and the European Pulse-Art program.

</section>

## Resources

{{< table "resources" >}}

## Axes of research

{{< axes >}}
