---
title: 'Marie-Claude Bossière'
date: 
weight: 3
description: "Pédopsychiatre"
thumbnail: ''
image: ''
jobtitle: 'Co-fondatrice de La Clinique Contributive'
type: "person"
activities:
    - "pmi"
    - "rne"
    - "en veille"
---