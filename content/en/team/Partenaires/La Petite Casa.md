---
title: 'La Petite Casa'
weight: 6
description: "Partenaire Cuisine Contributive"
thumbnail: ''
image: ''
jobtitle: ''
type: "group"
activities:
    - "cuisine"
links:
  - url: https://www.facebook.com/adelinetissotpetitecasa/?profile_tab_item_selected=photos&_rdr
    label: Site web
    icon: "fa fa-link"
---