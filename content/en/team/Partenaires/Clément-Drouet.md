---
title: 'Clément Drouet'
date: 
weight: 3
description: "Centre d'adaptation psycho-pédagogique de la ville de Paris"
thumbnail: ''
image: ''
jobtitle: 'Animateur de Raisonnons nos écrans'
type: person
activities:
    - "rne"
---