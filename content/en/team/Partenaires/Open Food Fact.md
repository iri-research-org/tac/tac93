---
title: 'Open Food Fact'
weight: 6
description: "Partenaire ContribAlim"
thumbnail: ''
image: ''
jobtitle: ''
type: "group"
activities:
    - "contribalim"
links:
  - url: https://fr.openfoodfacts.org/
    label: Site web
    icon: "fa fa-link"
---