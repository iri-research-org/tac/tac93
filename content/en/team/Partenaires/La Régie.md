---
title: 'La Régie'
weight: 6
description: "Partenaire Éco"
thumbnail: ''
image: ''
jobtitle: ''
type: "group"
activities:
    - "eco"
links:
  - url: https://www.facebook.com/RegiedeQuartiersdeSaintDenis/?locale=fr_FR
    label: Site web
    icon: "fa fa-link"
---