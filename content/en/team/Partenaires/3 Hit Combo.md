---
title: "Association 3 Hit Combo, porteuse du projet RennesCraft"
weight: 4
description: ""
thumbnail: ''
image: ''
jobtitle: "Chercheurs associés Urbanité Numérique en Jeux"
type: "group"
activities:
    - "unej"
icon: images/icons/icons8-design-100.png
links:
  - url: https://www.3hitcombo.fr/
    label: Site web
    icon: "fa fa-link"
---