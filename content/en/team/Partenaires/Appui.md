---
title: 'Appuii'
weight: 6
description: "Partenaire ContribAlim"
thumbnail: ''
image: ''
jobtitle: ''
type: "group"
activities:
    - "contribalim"
icon: images/icons/icons8-design-100.png
links:
  - url: https://appuii.wordpress.com/
    label: Site web
    icon: "fa fa-link"
---