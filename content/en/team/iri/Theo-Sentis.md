---
title: 'Théo Sentis'
date: 
weight: 1
description: "Institut de recherche et d'innovation"
thumbnail: ''
image: ''
jobtitle: 'Chef de projet Comité Eco'
links:
type: "person"
activities:
    - "eco"
    - "cuisinecontributive"
---