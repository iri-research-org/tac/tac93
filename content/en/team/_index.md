---
title: 'Contributeur.rice.s'
description: "Le Programme TAC n'existerait pas sans ses  contributeur.rice.s"
date: 2018-02-12T15:37:57+07:00
header_transparent: false
summary_type: "framework/summary/summary-team.html"

hero:
  headings:
    heading: "Contributeur.rice.s"
    sub_heading: "Le Programme TAC n'existerait pas sans ses contributeur.rice.s"
  background:
    background_image: "images/photos/contributors_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true
---