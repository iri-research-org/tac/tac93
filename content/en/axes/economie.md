---
title: "Contributory Economy"
description: "Finance contributory knowledge practices"
date: 2019-10-03
weight: 4
id: economie-de-la-contribution

header_transparent: true

menu:
  main:
    weight: 4
    parent: "axes"
    params:
      icon: "images/icons/icons8-bar-chart-100.png"

fa_icon: ""
icon: "images/icons/icons8-bar-chart-100.png"
thumbnail: "images/photos/thumbnails/economie.jpg"
image: ""

hero:
  headings:
    heading: "Contributory Economy"
    sub_heading: "Finance contributory knowledge practices"
  background:
    background_image: "images/photos/economie_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Resources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: L’économie contributive
        authors: Franck Cormerais, Vincent Puig, Olivier Landau et Maude Durbecker
        date: n.d
        link: https://lecoleduterrain.fr/maniere-de-faire/leconomie-contributive/
      - title: Séminaire monnaie
        authors: L'Institut de recherche et d'innovation
        date: 2022
        link: https://iri-ressources.org/collections/season-72.html
      - title: Économie de la contribution et gestion des bien commun
        authors: Clément Morlat, Théo Sentis, Olivier Landau, Anne Kunvari, Vincent Puig
        date: 2021
        link: https://anis-catalyst.org/imaginaire-communs/economie-de-la-contribution-et-gestion-des-biens-communs/#:~:text=Le%20commun%20sur%20lequel%20s,l'%C3%A9conomie%20de%20march%C3%A9%20globalis%C3%A9e
      - ressource: Pour une nouvelle critique de l'économie politique
        title: "Bernard Stiegler"
        authors: Internation ed. Stiegler
        date: 2012
        link: http://www.editions-galilee.fr/f/index.php?sp=liv&livre_id=3261

---

In a 2023 study, the OECD reported that 27% of jobs are seriously threatened by automation and AI, and three in five workers are worried about losing their jobs due to AI. The gig economy continues to progress at the benefit of large digital platforms (part of the Uber phenomenon). However, in the era of the Anthropocene and environmental challenges, this **time freed by automation** could be oriented towards the development of **common goods**. The cultivation of these common goods and knowledge practices is part of the IRI's set of contributory research projects. The objective of the contributory economy model, which the IRI has been experimenting since 2017 in Seine–Saint-Denis, is to allow everyone to freely develop capacities thanks to **contributory income**. This income makes it possible to support free and non-subordinate capacitation, offering robust integration into the territorial economy. This effort takes up certain principles of the **French intermittent model for artistic creation** and also promotes the production of common goods as in the context of open source free software.

Interpretations of this contributory model are currently taking shape through the [ECO Project](/activities/eco) with the development of initiatives supporting **a local currency**, which aims to support the ecological and social transition. The ECO Project is particularly involved in the field of food security, knowledge practices, and capacity building, where the IRI leads the [ContribAlim](/activities/contribalim) project with the support of ADEME. Through the ECO committee, ContribAlim offers contributory cooking workshops.

## Resources

{{< table "ressources" >}}

