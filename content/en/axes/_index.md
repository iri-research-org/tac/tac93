---
title: "Axes"
sub_heading: ""
date: 2018-02-10T11:52:18+07:00

header_transparent: true

hero:
  headings:
    heading: "Axes of research"
    sub_heading: ""
  background:
    background_image: "images/photos/axes/axes_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true
---

## Axes for systemic territorial action

Territoire Apprenant Contributif, or the Contributive Learning Territory, is the first experimental territory resulting from the IRI's research on the contributory economy, which aims to inspire other territorial initiatives. It is deployed over three axes, each bringing together a certain number of experimentation and research activities.