---
title: "Urban Contributory Design"
description: "For a digital urbanity beyond the smart city"
date: 2019-10-03
weight: 4
id: design-de-la-contribution-urbaine

header_transparent: true

menu:
  main:
    weight: 4
    parent: "axes"
    params:
      icon: "images/icons/icons8-color-palette-100.png"

fa_icon: ""
icon: "images/icons/icons8-color-palette-100.png"
thumbnail: "images/photos/designurbaine_background.jpg"
image: ""

hero:
  headings:
    heading: "Urban Contributory Design"
    sub_heading: "For a digital urbanity beyond the smart city"
  background:
    background_image: "images/photos/designurbaine_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Resources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: Pour une nouvelle urbanité à l’ère du numérique
        authors: Bernard Stiegler
        date: 2017
        link: '/pdfs/pour_une_nouvelle_urbanite.pdf'
      - title: "Colloque Le nouveau génie urbain "
        authors: L'Institut de recherche et d'innovation
        date: 2018
        link: https://enmi-conf.org/wp/enmi18/
      - title: "Le nouveau génie urbain"
        authors: Dir. Bernard Stiegler
        date: 2019
        link: https://boutique.fypeditions.com/products/le-nouveau-genie-urbain?srsltid=AfmBOooJSYd8NEf0VLbpdyPK8Bl6hxfG2hHxjYFuAmck_UI4Axh5G3bf
---


With new advances in **digital technologies and platforms**, we are experiencing an industrial revolution that is transforming production, understandings of work, and urban lifestyles. Digital capitalism, based on the global interconnection of people and intensive data processing, benefits web giants by extracting value from user activities. This data economy is manifested by so-called **“smart cities”**, which often conceal the domination of territories by extraterritorial logic, ignoring local authorities and the practices of residents. 

We are observing profound industrial changes:

- The digitalization of services and objects transforms urban infrastructures into “augmented spaces”.
- **Robotization and technologies like BIM** and 3D modeling are changing architectural design and the management of urban flows.
- Production is being relocalised closer to consumers, with the finalisation of objects entrusted to “FabLabs” and “TechShops”, linking 4.0 factories and centralized production.

These transformations in the midst of the climate crisis require an analysis of their environmental, urban and societal impacts. Although they risk leading to a standardization of urban lifestyles and technological domination, **they also offer opportunities for new forms of ecologies and urban intelligence**. The IRI hopes to materialise this latter perspective by proposing territorial, digital and social experimentation projects.

## Resources

{{< table "ressources" >}}