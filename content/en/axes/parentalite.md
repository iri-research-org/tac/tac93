---
title: "Parenting, care, and the digital"
description: "Taking care of the digital and future generations"
date: 2019-10-03
weight: 2
id: parentalite-soin-numerique

header_transparent: true

menu:
  main:
    weight: 2
    parent: "axes"
    params:
      icon: "/images/icons/icons8-merge-git-100.png"

fa_icon: ""
icon: "/images/icons/icons8-merge-git-100.png"
thumbnail: "/images/photos/thumbnails/rne.jpg"
image: ""

hero:
  headings:
    heading: "Parenting, care, and the digital"
    sub_heading: "Taking care of the digital and future generations"
  background:
    background_image: "/images/photos/parentalite_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Resources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: "Livret Écrans (Screen Booklet)"
        authors: "The city of Saint Denis with the participation of the Association En Veille"
        date: 2024
        link: https://www.calameo.com/ville-saint-denis/read/0003435242149a8b77e0a?page=9
      - title: "L’exposition précoce et excessive aux écrans (EPEE) : un nouveau syndrome (Early and excessive exposure to screens (EPEE): a new syndrome)"
        authors: D. Marcelli, MC. Bossière, AL. Ducanda
        date: 2020
        link: https://shs.cairn.info/revue-devenir-2020-2-page-119?lang=fr
      - title: "Guide Méthodologique (Methodological Guide)"
        authors: "L'Institut de recherche et d'innovation"
        date: 2023
        link: https://cloud.svc.iri-research.org/sites/clinique_contributive/sub/guide%20methodologique#page-toc-8
      - title: "Le bébé au temps du numérique (The baby in the digital age)"
        authors: Marie-Claude Bossière
        date: 2021
        link: https://www.editions-hermann.fr/livre/le-bebe-au-temps-du-numerique-marie-claude-bossiere
      - title: "Prendre soin de la jeunesse et des générations (Take Care of the Youth and of Future Generations)"
        authors: Bernard Stiegler
        date: 2010
        link: https://www.philomag.com/livres/prendre-soin-tome-1-de-la-jeunesse-et-des-generations


---

In the book entitled *Prendre soin de la jeunesse et des générations (Take Care of the Youth and of Future Generations)*, Bernard Stiegler describes the importance of **care** on a global scale: everyone has the responsibility to preserve collective social and political life. The book states that digital technology can offer the possibility of reinventing collective intelligence. The collective group is presented as that which can restore our hope in the future, opening up education between peers, and allowing the creation and dissemination of new knowledge.

The *Parenting, care, and the digital* research axis is an initiative seeking to investigate **the effects of digital technology on our daily lives, our families, social lives, health, and futures.** The research also aims to find possible solutions to screen overexposure and its effects on the development of young children in Seine-Saint-Denis. Scientific studies demonstrate the **toxic effects** of screen overexposure (smartphones, tablets, computers, television) among very young children aged 0-3, including delays in language, learning, motor skills, as well as the development of behavioral disorders. This research addresses health professionals, to facilitate insights for them to help families and parents. These efforts assist with reconnecting parents with their children, raising awareness among other parents, and developing actions and projects on the territory.

## Resources

{{< table "ressources" >}}