---
title: "Design de la contribution urbaine"
description: "Pour une urbanité numérique au-delà de la smart city"
date: 2019-10-03
weight: 4
id: design-de-la-contribution-urbaine

header_transparent: true

menu:
  main:
    weight: 4
    parent: "axes"
    params:
      icon: "images/icons/icons8-color-palette-100.png"

fa_icon: ""
icon: "images/icons/icons8-color-palette-100.png"
thumbnail: "images/photos/designurbaine_background.jpg"
image: ""

hero:
  headings:
    heading: "Design de la contribution urbaine"
    sub_heading: "Pour une urbanité numérique au-delà de la smart city"
  background:
    background_image: "images/photos/designurbaine_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Ressources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: Pour une nouvelle urbanité à l’ère du numérique
        authors: Bernard Stiegler
        date: 2017
        link: '/pdfs/pour_une_nouvelle_urbanite.pdf'
      - title: "Colloque Le nouveau génie urbain "
        authors: L'Institut de recherche et d'innovation
        date: 2018
        link: https://enmi-conf.org/wp/enmi18/
      - title: "Le nouveau génie urbain"
        authors: Dir. Bernard Stiegler
        date: 2019
        link: https://boutique.fypeditions.com/products/le-nouveau-genie-urbain?srsltid=AfmBOooJSYd8NEf0VLbpdyPK8Bl6hxfG2hHxjYFuAmck_UI4Axh5G3bf
---


Avec les avancées des **technologies numériques et des plateformes**, nous vivons une révolution industrielle qui transforme la production, l'organisation du travail et les modes de vie urbains. Le capitalisme numérique, basé sur l'interconnexion mondiale des individus et le traitement intensif des données, profite aux géants du web en extrayant de la valeur des activités des utilisateurs. Cette économie des données se manifeste par les dites **« smart cities »**, qui dissimulent souvent la domination des territoires par des logiques extraterritoriales, ignorant les autorités locales et les pratiques des habitants. 

Nous observons des mutations industrielles profondes :

- La digitalisation des services et des objets transforme les infrastructures urbaines en « espaces augmentés ».
- La **robotisation et les technologies comme le BIM** et le modélisation 3D changent la conception architecturale et la gestion des flux urbains.
- La production se relocalise près des consommateurs, avec des tâches de finition confiées aux « FabLabs » et « TechShops », reliant usines 4.0 et production centralisée.

Ces transformations, en pleine crise climatique, nécessitent une analyse de leurs impacts environnementaux, urbanistiques et sociétaux. Bien qu’elles risquent de conduire à une standardisation des modes de vie urbains et à la domination technologique, **elles offrent aussi des opportunités pour de nouvelles formes d’écologies et d’intelligences urbaines**. L’IRI s’inscrit dans cette optique en proposant des projets d’expérimentation territoriale, numérique et sociale.

## Ressources

{{< table "ressources" >}}