---
title: "Économie de la contribution"
description: "Soutenir les savoirs comme condition de la contribution"
date: 2019-10-03
weight: 4
id: economie-de-la-contribution

header_transparent: true

menu:
  main:
    weight: 4
    parent: "axes"
    params:
      icon: "images/icons/icons8-bar-chart-100.png"

fa_icon: ""
icon: "images/icons/icons8-bar-chart-100.png"
thumbnail: "images/photos/thumbnails/economie.jpg"
image: ""

hero:
  headings:
    heading: "Économie de la contribution"
    sub_heading: "Soutenir les savoirs comme condition de la contribution"
  background:
    background_image: "images/photos/economie_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Ressources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: L’économie contributive
        authors: Franck Cormerais, Vincent Puig, Olivier Landau et Maude Durbecker
        date: n.d
        link: https://lecoleduterrain.fr/maniere-de-faire/leconomie-contributive/
      - title: Séminaire monnaie
        authors: L'Institut de recherche et d'innovation
        date: 2022
        link: https://iri-ressources.org/collections/season-72.html
      - title: Économie de la contribution et gestion des bien commun
        authors: Clément Morlat, Théo Sentis, Olivier Landau, Anne Kunvari, Vincent Puig
        date: 2021
        link: https://anis-catalyst.org/imaginaire-communs/economie-de-la-contribution-et-gestion-des-biens-communs/#:~:text=Le%20commun%20sur%20lequel%20s,l'%C3%A9conomie%20de%20march%C3%A9%20globalis%C3%A9e
      - ressource: Pour une nouvelle critique de l'économie politique
        title: "Bernard Stiegler"
        authors: Internation ed. Stiegler
        date: 2012
        link: http://www.editions-galilee.fr/f/index.php?sp=liv&livre_id=3261

---


À l’heure où l’OCDE a notifié dans sa dernière étude de 2023 que 27 % des emplois sont fortement menacés par l’automatisation et par l’IA et que trois travailleurs sur cinq s’inquiètent de perdre leur emploi du fait de l’IA, le travail hors emploi ne cesse de progresser au profit des grandes plateformes numériques (phénomène d’ubérisation) alors que — à l’ère de l’anthropocène et des défis environnementaux — ce **temps libéré par l’automatisation** pourrait être orienté vers le développement de **biens communs** en l’occurrence des savoirs cultivés dans le cadre de projets que nous nommons de recherche contributive. C’est tout l’objectif du modèle de l’économie contributive que l’IRI expérimente depuis 2017 en Seine–Saint-Denis et qui doit permettre à chacun de développer librement ses savoirs grâce à un **revenu contributif**. Ce revenu, conditionnel, permet de soutenir la capacitation libre et non-subordonnée qui devient un levier pour une meilleure insertion dans l’économie territoriale. Cette articulation reprend certains principes du régime des **intermittents du spectacle** et favorise par ailleurs la production de biens communs comme dans le contexte du logiciel libre. 

Une interprétation particulière de ce modèle prend forme actuellement à travers le projet [ECO](/activities/eco) avec le développement d’**une monnaie locale** soutenant des initiatives de contribution à la transition écologique et sociale notamment dans le champ de l’alimentation où l’IRI conduit le projet [ContribAlim](/activities/contribalim) avec le soutien de l’ADEME et propose avec le Comité ECO des ateliers de cuisine contributive.

## Ressources

{{< table "ressources" >}}

