---
title: "Axes"
sub_heading: ""
date: 2018-02-10T11:52:18+07:00

header_transparent: true

hero:
  headings:
    heading: "Axes de recherche"
    sub_heading: ""
  background:
    background_image: "images/photos/axes/axes_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true
---

## Axes pour une action territoriale systémique

Territoire Apprenant Contributif est le premier territoire d’expérimentation issu des travaux de l’IRI sur la recherche et l’économie contributive et qui a vocation à inspirer d’autres initiatives territoriales. Il se déploie sur trois axes, regroupant chacun un certain nombre d’activités d’expérimentation et de recherche.