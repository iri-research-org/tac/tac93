---
title: "Parentalité, soin, et numérique"
description: "Prendre soin du numérique et des générations"
date: 2019-10-03
weight: 2
id: parentalite-soin-numerique

header_transparent: true

menu:
  main:
    weight: 2
    parent: "axes"
    params:
      icon: "/images/icons/icons8-merge-git-100.png"

fa_icon: ""
icon: "/images/icons/icons8-merge-git-100.png"
thumbnail: "/images/photos/thumbnails/rne.jpg"
image: ""

hero:
  headings:
    heading: "Parentalité, soin, et numérique"
    sub_heading: "Prendre soin du numérique et des générations"
  background:
    background_image: "/images/photos/parentalite_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

tables:
  ressources:
    name: "Ressources"

    # table columns (required)
    cols:
      - id: "title"
        name: "Titre"
        tip: "Title"
      - id: "authors"
        name: "Auteur·e·s"
        tip: "a list of the authors"
        align: "left" # left-align column
      - id: "date"
        name: "Date"
      - id: link
        name: "Lien"

    # table rows
    rows:
      - title: "Livret Écrans"
        authors: "La ville de Saint Denis avec la participation de l'Association En Veille"
        date: 2024
        link: https://www.calameo.com/ville-saint-denis/read/0003435242149a8b77e0a?page=9
      - title: "L’exposition précoce et excessive aux écrans (EPEE) : un nouveau syndrome"
        authors: D. Marcelli, MC. Bossière, AL. Ducanda
        date: 2020
        link: https://shs.cairn.info/revue-devenir-2020-2-page-119?lang=fr
      - title: "Guide Méthodologique"
        authors: "L'Institut de recherche et d'innovation"
        date: 2023
        link: https://cloud.svc.iri-research.org/sites/clinique_contributive/sub/guide%20methodologique#page-toc-8
      - title: "Le bébé au temps du numérique"
        authors: Marie-Claude Bossière
        date: 2021
        link: https://www.editions-hermann.fr/livre/le-bebe-au-temps-du-numerique-marie-claude-bossiere
      - title: "Prendre soin de la jeunesse et des générations"
        authors: Bernard Stiegler
        date: 2010
        link: https://www.philomag.com/livres/prendre-soin-tome-1-de-la-jeunesse-et-des-generations


---



Dans un livre intitulé *Prendre soin de la jeunesse et des générations*, Bernard Stiegler décrit l’importance du **soin**, à échelle planétaire : chacun a la responsabilité de préserver la possibilité d’une vie collective, sociale et politique. Il pose que le numérique n’est pas une fatalité, et peut constituer une possibilité de réinventer l’intelligence collective. Le groupe est présenté comme ce qui peut redonner espoir dans l’avenir, ouvrant une éducation entre pairs, permettant la création et la diffusion de nouveaux savoirs. 

L’axe de recherche *Parentalité, soin et numérique* est une initiative qui tente d’étudier **les effets du numérique sur notre vie quotidienne, nos liens familiaux, sociaux, notre santé**, notre futur. Il s’agit aussi d’essayer de trouver des pistes de solutions à la problématique de la surexposition aux écrans et ses effets sur le développement des jeunes enfants en Seine-Saint-Denis. Les études scientifiques démontrent les **effets très toxiques** (retard de langage, des apprentissages, de la motricité et troubles du comportement) de la surexposition des très jeunes enfants 0-3 ans aux écrans (smartphones, tablettes, ordinateurs, télévision). L’idée est de s’adresser aux professionnels de santé afin qu’ils puissent aider les familles, et aux parents, afin qu’ils puissent retisser un lien avec leur enfant, sensibiliser d’autres parents, et inventer des actions et des projets sur le territoire.

## Ressources

{{< table "ressources" >}}