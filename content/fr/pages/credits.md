---
title: 'Credits'
date: 2025-01-06
---


- Vincent Puig
- Elvira Hojberg
- Anne Alombert
- Maude Durbecker
- Riwad Salim
- Marie-Claude Bossière
- Théo Sentis
- Anne Kunvari
- Bernard Stiegler
- Sarah Grinalds
- Guillaume Pellerin