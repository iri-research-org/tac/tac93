---
title: 'Mentions légales'
date: 2024-11-29
menu:
  bottom:
---

Date de la dernière mise à jour : 29/11/2024

## Informations légales

Le site « tac93.fr » ainsi que tous les éléments le composant et notamment : l’ensemble des textes, les photographies, vidéos et documents sonores sont la propriété de l’IRI ou d’un tiers dont il a été obtenu les droits d’exploitation. Ils ne peuvent donc être utilisés sans autorisation préalable au titre des dispositions du Code de la propriété intellectuelle.

En conséquence, toute représentation ou reproduction intégrale ou partielle, toute utilisation, adaptation, transformation, arrangement, de tout ou partie d’œuvres, de signes distinctifs ou de bases de données, protégés par un droit de propriété intellectuelle de l'IRI, faite sans son consentement est illicite, et constitue une contrefaçon sanctionnée pénalement selon l’article L 122-4 du Code la Propriété Intellectuelle.

## Informations techniques

L'IRI est le propriétaire et l'hébergeur du site internet « tac93.fr ».

Ce site est créé avec [Hugo](https://gohugo.io/).

## Informations éditoriales


### Directeur de la publication

Olivier Landau

### Responsable éditorial

Vincent Puig

### Chef de projet

Guillaume Pellerin

### Conception graphique et technique

Robert Austin, Guillaume Pellerin

### Edition

Elvira Hojberg

## Coordonnées

### Editeur

IRI - Institut de Recherche et Innovation
4 rue Aubry le Boucher 75004 Paris, France

SIRET : 50892348900019

RNA : W751187560

Téléphone : +33 183 87 63 25

### Hébergeur

Centre Pompidou - Place Georges-Pompidou, 75004 Paris

Téléphone : 01 44 78 12 33
