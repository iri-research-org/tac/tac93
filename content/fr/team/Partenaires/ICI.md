---
title: 'ICI !, Initiatives construites Îlo-Dionysiennes'
weight: 3
description: ""
thumbnail: ''
image: ''
jobtitle: "Chercheurs associés Urbanité Numérique en Jeux"
type: "group"
activities:
    - "unej"
links:
  - url: https://www.associationici.com/
    label: Site web
    icon: "fa fa-link"
---