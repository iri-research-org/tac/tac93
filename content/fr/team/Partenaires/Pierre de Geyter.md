---
title: 'Élèves et professeurs du collège Pierre de Geyter à Saint-Denis'
weight: 5
description: "Académie de Créteil"
thumbnail: ''
image: ''
jobtitle: "Chercheurs associés Urbanité Numérique en Jeux"
type: "group"
activities:
    - "unej"
---