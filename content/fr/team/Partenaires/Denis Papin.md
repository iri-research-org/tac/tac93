---
title: 'Élèves et professeurs du lycée professionnel Denis Papin à La Courneuve'
weight: 5
description: "Académie de Créteil"
thumbnail: ''
image: ''
jobtitle: "Chercheurs associés Urbanité Numérique en Jeux"
type: "group"
activities:
    - "unej"
---