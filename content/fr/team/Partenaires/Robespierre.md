---
title: 'Élèves et professeurs du collège Robespierre à Épinay-sur-Seine'
weight: 5
description: "Académie de Créteil"
thumbnail: ''
image: ''
jobtitle: "Chercheurs associés Urbanité Numérique en Jeux"
type: "group"
activities:
    - "unej"
---