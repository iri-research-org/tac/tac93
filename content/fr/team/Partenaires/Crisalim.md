---
title: 'Crisalim'
weight: 6
description: "Partenaire ContribAlim"
thumbnail: ''
image: ''
jobtitle: ''
type: "group"
activities:
    - "contribalim"
links:
  - url: https://www.crisalim.co/
    label: Site web
    icon: "fa fa-link"
---