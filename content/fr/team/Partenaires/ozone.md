---
title: "O’zone"
weight: 4
description: "Cabinet d’architecture et d’urbanisme"
thumbnail: ''
image: 'images/logo/OZONE_logo.png'
jobtitle: "Chercheurs associés"
type: "group"
# logo: "images/logo/OZONE_logo.png"
activities:
    - "unej"
    - "unej2"
icon: images/icons/icons8-design-100.png
links:
  - url: http://www.ozone-architectures.com/
    label: Site web
    icon: "fa fa-link"

---