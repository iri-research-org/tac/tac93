---
title: "Présentation du projet UNEJ lors des ENMI 2020"
date: 2020-12-23
authors: ["IRI"]
categories: ["Séminaire"]
activity: "unej"
description: "Maxime Barilleau présente le projet UNEJ."
thumbnail: "images/photos/events/201223/201223_thumbnail.png"
image: "images/photos/events/201223/201223_background.png"
---

Lors des Entretiens du Nouveau Monde Industriel, en décembre 2020, Maxime Barilleau, enseignant au collège Raymond Poincaré, a présenté les enjeux pédagogiques du projet UNEJ. Vous trouverez la session intitulée : « Éducation et capacitation dans l’ère post-véridique : technologies numériques, médias sociaux et savoirs transgénérationnels » dans la vidéo 

{{< framework/video url="https://ldt.iri.centrepompidou.fr/ldtplatform/ldt/embed/v4/iframe?content_id=e57ac5f5-54c1-11eb-aff6-00145ea4a2be&project_id=002ec70b-54c3-11eb-aff6-00145ea4a2be&polemic=tweet&polemic_version=2&polemic_max_elements=50&annotations_list=True&annotation=True&tweet=True&segments_annotation_types=chap,découpage&multisegments=True&sparkline=True&tagcloud=False&autostart=True&concatenate_projects=True#id=0' seamless='seamless'  frameborder='0' width='600' height='600'" >}}


