---
title: 'Contact'
date: 2018-02-22T17:01:34+07:00
contact:
  phone: "+33 183 87 63 25"
  email: "contact@iri.centrepompidou.fr"
  address: "4 rue Aubry le Boucher 75004 Paris, France"
  google_map_text_link: https://maps.app.goo.gl/ryx2TEy9SVS4YSXs8
locations:
  - title: Institut de Recherche et Innovation
    phone: "+33 183 87 63 25"
    email: contact@iri.centrepompidou.fr
    address: "4 rue Aubry le Boucher 75004 Paris, France"
    google_map_text_link: https://maps.app.goo.gl/ryx2TEy9SVS4YSXs8
    google_map_embed_link: "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d328.1108097331304!2d2.3505568!3d48.8603751!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e671be4e863433%3A0xfad4e9a29f9dfe90!2sInstitut%20de%20recherche%20et%20d%27innovation%20(IRI)%20-%20Centre%20Pompidou!5e0!3m2!1sfr!2sfr!4v1732287839961!5m2!1sfr!2sfr"
---

