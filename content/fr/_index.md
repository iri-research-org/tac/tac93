---
title: "Territoire Apprenant Contributif"
date: 2019-02-22
description: "Le programme TAC est une expérimentation territoriale de recherche qui vise à préparer l'économie de la contribution."
header_transparent: true
hero:
  type: hero
  enabled: true
  options:
    paddingTop: ""
    paddingBottom: false
    borderTop: false
    borderBottom: false
    theme: primary
    classes: "my-custom-class another-custom-class"
  align_horizontal: left
  align_vertical: middle
  height: 600px
  fullscreen_mobile: true
  fullscreen_desktop: false
  headings:
    heading: Territoire Apprenant Contributif
    sub_heading: Le programme Territoire Apprenant Contributif (TAC) est un programme de recherche contributive de longue durée qui réunit habitants, professionnels et chercheurs, afin de cultiver de nouveaux savoirs dans le cadre d’une économie qui soutient la contribution à une bifurcation durable.
    text: ''
    text_color: "#FFFFFF"
    text_color_dark: "#FFFFFF"
  background:
    background_image: "images/photos/landing_background.jpg"
    background_image_blend_mode: "overlay" # "overlay", "multiply", "screen"
    background_gradient: true
    background_color: "" # "#030202"
    background_color_dark: "" # "#030202" 
    opacity: 1
  image:
    image: false
    shadow: false
    border: false
    alt: ""
  buttons:
    - button:
      text: "En savoir plus"
      url: "/programme/"
      external: false
      fa_icon: false
      outline: true
      style: "transparent"
axes:
  enabled: true
  show_view_all: true
  sort_by: "weight" # "date" "weight"
  limit: 3
  label: "Axes de recherche"
intro:
  enabled: true
  align: left
  image: "images/photos/activities/pmi_background.jpeg"
  heading: "Favoriser l'intelligence collective pour des territoires plus résilients"
  description: "Le programme TAC est composé de nombreuses activités fondées sur des principes de contribution et de valorisation des savoir-faire, des savoir-être et des savoir-théoriser."
  buttons:
    - button:
      text: "Découvrir nos activités"
      url: "/activities/"
      external: false
      fa_icon: false
      outline: true
      style: "primary"
  partners:
    enabled: true
activities:
  enabled: true
  label: "Quelques Activités"
  show_view_all: false
  limit: 6
outro:
  enabled: true
  align: center
  image: ""
  heading: Témoigner, rencontrer, contribuer
  description: Si vous désirez accélérer la transformation sociale, écologique et économique de votre territoire
  buttons:
    - button:
      text: "Contactez-nous"
      url: "contact/"
      external: false
      fa_icon: false
      outline: false
      style: ""
blog:
  enabled: true
  show_view_all: false
  limit: 3
---
