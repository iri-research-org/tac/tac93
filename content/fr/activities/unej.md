---
title: "Urbanités numériques en jeux #1"
description: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec les établissements scolaires de la Seine Saint-Denis."
period: 2019-2024
date: 2024-11-20
weight: 1
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Urbanités numériques en jeux #1"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec les établissements scolaires de la Seine Saint-Denis."

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/unej.png"
icon: ""

hero:
  headings:
    heading: "Urbanités numériques en jeux #1"
    sub_heading: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec les établissements scolaires de la Seine Saint-Denis."
  background:
    background_image: "images/photos/activities/unej_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content:
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Cabinet d’architecture et d’urbanisme O’zone"
      link: http://www.ozone-architectures.com/
    - name: "ICI !, Initiatives construites Îlo-Dionysiennes (association d’architectes)"
      link: https://www.associationici.com/
    - name: "Association 3 Hit Combo, porteuse du projet RennesCraft"
      link: https://www.3hitcombo.fr/rennescraft/
    - name: "Rectorat de l’Académie de Créteil"
      link: https://www.ac-creteil.fr/
    - name: "IGN"
      link: https://www.ign.fr/
    - name: "CDOS 93 (Comité Départemental Olympique et Sportif de Seine-Saint-Denis)"
      link: https://www.cdos93.org/
  text: "UNEJ vise à capaciter des enseignants et leurs élèves pour faire des propositions d’aménagement de leur établissement et de leur quartier. 12 lycées et collèges de Seine-Saint-Denis inventent le futur de leur ville et du Village Olympique hérité en s’appuyant sur une version libre et open source de Minecraft."
  link_label: Site du projet
  link_url: "https://unej.tac93.fr/"
  fundings: 
    - name: "Caisse des Dépôts et Consignation"
      link: https://www.caissedesdepots.fr/
    - name: "Solidéo"
      link: https://www.ouvrages-olympiques.fr/
    - name: "Ministère de l’éducation nationale"
      link: https://www.education.gouv.fr/
    - name: "Fondation de France"
      link: https://www.fondationdefrance.org/fr/
    - name: "Fondation SNCF"
      link: https://www.groupe-sncf.com/fr/engagements/mecenat-sponsoring/fondation
    - name: Département de la Seine Saint-Denis
      link: https://seinesaintdenis.fr/
    - name: Fondation NextInternet
      link: https://nlnet.nl/project/MinetestEdu/
---


L’IRI et ses partenaires s’interrogent sur **le futur de la ville à l’époque des plateformes et des nouvelles technologies du bâtiment**. Des expériences sur la ville telle que l’expérience Rennecraft utilisant à l’époque le jeu vidéo Minecraft et les données du Service d'Information Géographique de la ville de Rennes nous ont beaucoup inspirés.

Démarrés en partenariat avec le rectorat de Créteil, les ateliers UNEJ sont élaborés avec des architectes, urbanistes, enseignants, chercheurs et amateurs du jeu vidéo, ils se déroulent sur le temps scolaire et sont animés par des enseignants volontaires et formés, et par l’équipe d’intervenants (architectes, urbanistes, designers numériques) qui assurent 3 à 6 interventions par établissement par an sur 4 ans (2020-2024). Ils s’appuient sur la pratique du **jeu vidéo [Luanti](https://www.luanti.org/)** (version libre de Minecraft, anciennement Minetest) pour accompagner les élèves dans la conception et la modélisation des propositions en lien avec l’aménagement de leur territoire, en s’intégrant dans **la dynamique des Jeux Olympiques et Paralympiques** jusqu’à fin 2024.

![les eleves jouent au minetest](/images/photos/blur/unej_text1.jpg)

En 2020-2021, les élèves ont travaillé sur des réaménagements de leur cour de récréation, en 2021-2022, sur le quartier de l'établissement et en 2023-2024, sur le village des Athlètes et le village des Médias, construits dans le cadre des Jeux Olympiques de 2024. Les établissements ont travaillé sur l’aménagement du Mail Finot, espace au cœur du village olympique.  Les élèves ont appris à mieux connaître leur territoire, et ont découvert des métiers comme urbaniste, architecte et constructeur. Les élèves ont appris à **coopérer, débattre et exprimer leurs idées**, tout en modélisant des propositions d'aménagement de leur territoire.

{{< framework/video url="https://media.iri.centrepompidou.fr/video/unej/expo%20UNEJ/VILLAGE%20AQUATIQUE.mp4">}}


Vous pouvez retrouver toute l’histoire du projet, les informations et modélisations des élèves sur [le site entièrement dédié au projet]( https://unej.tac93.fr/).


Le dispositif s’est appuyé sur des apports pédagogiques variés pour traiter les problématiques d’aménagement. Les élèves ont progressivement élargi leur champ d’action : après avoir travaillé sur leur cour de récréation, ils se sont intéressés à leur quartier pour finalement se pencher sur le village Olympique et Paralympique.

Les disciplines les plus sollicitées ont été les Mathématiques, la Technologie et l’Histoire-Géographie, souvent complétées par la SVT, la Physique-Chimie et le Français selon les thématiques abordées, comme l’écologie, la gestion de l’eau ou l’élaboration d’un argumentaire pour présenter ses idées.

![méthode unej](/images/photos/unej/unej_methode.jpg)

L’usage de Luanti a favorisé la collaboration entre élèves, l’entraide, et le développement de différents savoirs. La méthodologie, élaborée par O’zone, ICI! et l’IRI, s’est articulée autour de trois phases distinctes :

1. Les élèves ont pris en main l’outil numérique, exploré numériquement le territoire et visité physiquement l’espace à aménager pour en identifier les enjeux.

2. Après avoir reconstruit le territoire sur Luanti à partir de plans d’aménagement fournis par les urbanistes, les élèves ont réfléchi collectivement aux problématiques à traiter — écologiques, sociales ou esthétiques. Cette réflexion a abouti à la modélisation de leurs propositions, qu’ils ont ensuite présentées devant un jury.

3. Enfin, ils ont modélisé leurs propositions, préparé une présentation et restitué leur travail devant un jury.

Cette méthodologie structurée, qui allie exploration, réflexion collective et modélisation, a été déclinée dans des formats variés pour s’adapter aux besoins et aux contraintes des établissements scolaires participants.

## Dispositif Technique

Luanti a été l’élément clé du dispositif grâce à sa capacité à être pris en main très rapidement et à ses fonctionnalités, comparables à une pratique numérique des legos. Cet outil libre et gratuit, équivalent de Minecraft – jusqu’à récemment considéré comme le jeu vidéo le plus téléchargé au monde –, est facilement accessible à de jeunes élèves, souvent déjà familiers avec ce type de jeux. Ce choix s’est imposé à la suite d’un travail de veille technologique collective et contributive, amorcé avec Thomas François, l’un des fondateurs du projet Rennescraft. Si Minecraft permet de mobiliser des connaissances acquises hors du cadre scolaire, son statut de logiciel payant et propriétaire, ainsi que les limites de sa version éducative, freinaient son usage en classe. À l’inverse, Luanti, libre et modulaire, répondait bien mieux aux attentes pédagogiques par sa gratuité, sa flexibilité et sa faible consommation en ressources informatiques.

{{< framework/video url="https://media.iri.centrepompidou.fr/video/unej/expo%20biennale%20saint-etienne%202022/ITW%20POINCAR%C3%89%20-%20Saint-Etienne%20-%20SUB.mp4">}} 

Dans le cadre du projet UNEJ, les données géographiques ont été transformées en cartes de voxels 3D grâce au service [« Minecraft à la carte »](https://minecraft.ign.fr/) de l’IGN, permettant de représenter le territoire de la Seine-Saint-Denis. En partenariat avec l’IGN, une carte couvrant tout le département a été générée, puis hébergée sur un serveur maintenu par l’IRI. Chaque établissement scolaire participant disposait d’un accès privé à une zone dédiée de cette carte. En complément, une version en deux dimensions de la carte était mise à disposition pour faciliter la localisation des zones et la préparation des activités. Par ailleurs, l’IRI a fourni des mods adaptés pour aider les enseignant.e.s à naviguer dans la carte, gérer les sessions de jeu et répondre aux besoins pédagogiques spécifiques de leurs élèves.

Pour en savoir plus sur le dispositif, vous pouvez voir ce [tutoriel détaillé](/unej-doc/)

## Formation des enseignant.e.s

Depuis 2020/2021, trois journées de formation annuelles ont été organisées à l’initiative du rectorat de Créteil pour accompagner les enseignant.e.s dans le projet UNEJ. Ces formations visaient à renforcer leurs compétences théoriques et pratiques, à développer des séquences pédagogiques et à collaborer avec des architectes référents.

![formation d'enseignants](/images/photos/unej/unej_formation.JPG)

Première année : bases théoriques et découverte de Minetest
La première année a posé les fondations, avec des interventions sur les enjeux numériques des villes (Dominique Boullier, Vincent Puig) et les politiques d’aménagement sportif (CDOS93). Les enseignant.e.s ont exploré la carte voxel de la Seine-Saint-Denis et acquis les bases nécessaires pour intégrer Minetest dans leurs projets éducatifs.

Deuxième année : pratiques et projets concrets
Les formations se sont orientées vers des ateliers pratiques, comme la conception d’aménagements de cours de récréation, et des échanges d'expériences. Des spécialistes telle que Valérie Peugeot ont apporté des éclairages innovants, tandis que les enseignant.e.s ont approfondi leurs connaissances des outils numériques qui ont constitué le projet.

Années suivantes : perfectionnement et bilan
Les années suivantes ont approfondi les pratiques avec des focus sur la programmation (Python avec Minetest), les outils professionnels (maquette BIM du VOP), et l’évaluation des apprentissages. La dernière année a permis un bilan global des projets et a mis en avant la pérennisation des ressources éducatives libres et des contributions des enseignant.e.s au-delà du projet.

## Ateliers scolaires

Trois modes d’organisation ont été mis en place pour s’adapter aux contraintes des établissements. Certains ateliers ont pris la forme de séances bi-hebdomadaires de deux heures avec les mêmes classes tout au long du projet. D’autres ont fonctionné sous forme de clubs hors temps scolaire, ouverts sur inscription et souvent reconduits d’une année sur l’autre. Enfin, des ateliers intensifs ont été organisés sur deux ou trois jours lors de semaines dédiées en fin de trimestre.

Les architectes et enseignant.e.s ont travaillé en étroite collaboration pour optimiser le déroulement des ateliers. Ils ont conclu qu’il était plus pertinent de viser des objectifs annuels reconductibles, plutôt qu’un planning pluriannuel.

![balade urbaine](/images/photos/unej/UNEJ-page004.jpeg)

Projets thématiques

La cour de récréation
Chaque établissement a travaillé sur sa cour en utilisant les méthodes proposées par les architectes référents. Les élèves ont mené des visites, enquêtes et observations avant de formuler des propositions d’aménagement inspirées des projets de cour « Oasis », visant à rendre ces espaces plus accueillants et adaptés.

Le quartier proche
En collaboration avec les collectivités locales, les élèves ont exploré leur environnement immédiat. Ils ont participé à des concertations urbaines, rencontré des élus, visité des espaces publics et intégré ces expériences dans leurs modélisations.

Le village olympique et paralympique
Certains établissements ont orienté leurs travaux sur des espaces liés à l’héritage des Jeux Olympiques. Le collège Dora Maar, situé près du Village Olympique, s’est concentré sur l’aménagement du mail Finot. Par ailleurs, un atelier de programmation Python a permis à des élèves de développer des outils pour enrichir leurs modélisations.

![atelier village olympique](/images/photos/unej/UNEJ-page006.jpeg)

Modélisation collective
Lors de la deuxième année, un atelier collaboratif a réuni plusieurs établissements autour du thème de l’eau. Guidés par des architectes, les élèves ont imaginé ensemble des aménagements pour les bords de Seine, intégrant des enjeux d’écologie et d’urbanisme.

![modélisation village aquatique](/images/photos/unej/UNEJ-page014.jpeg)

Restitution finale
Une restitution a été organisée à la serre Wangari pour clôturer ces quatre années de travail. Les élèves ont finalisé leurs projets avant de les présenter aux Archives Nationales, mettant en lumière leurs idées et réalisations.

![restitution finale](/images/photos/unej/UNEJ-page008.jpeg)

### Galerie des projets

{{< image-gallery gallery_dir="/images/photos/unej/gallery" >}}

<br>
### Vidéo présentation d'UNEJ (en deux parties)

