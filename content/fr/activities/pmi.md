---
title: "Ateliers de la PMI Pierre Semard"
description: "Une recherche contributive pour de nouveaux savoirs sur la surexposition aux écrans"
date: 2024-11-20
weight: 2
portfolio: ["research"]
axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Ateliers de la PMI Pierre Semard"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Une recherche contributive pour de nouveaux savoirs sur la surexposition aux écrans"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/pmi.jpeg"
icon: ""

hero:
  headings:
    heading: "Ateliers de la PMI Pierre Semard"
    sub_heading: "Une recherche contributive pour de nouveaux savoirs sur la surexposition aux écrans"
  background:
    background_image: "images/photos/activities/pmi_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: "2017-"
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "PMl Pierre Sémard"
      link: 
    - name: "CNRS"
      link: https://republique-des-savoirs.fr/en/home/
    - name: "Ville de Saint Denis"
      link: https://ville-saint-denis.fr/
  text: "Depuis 2017, chercheurs, professionnels de la PMI Pierre Sémard et parents se réunissent chaque semaine pour des ateliers pratiques ou théoriques de capacitation collective sur la surexposition aux écrans des tout-petits enfants à travers différents dispositifs : jeux, jardinage, écriture."
  link_label: Wiki de la Clinique Contributive
  link_url: "https://cloud.svc.iri-research.org/sites/clinique_contributive/"
  fundings: 
    - name: "Fondation de France"
      link: https://www.fondationdefrance.org/fr/
---


L’omniprésence des écrans et la surexposition qui l’accompagne est aujourd’hui un problème de santé public de plus en plus reconnu ([en témoigne la commission mise en place par le président Macron au début de l’année 2024]( https://www.info.gouv.fr/actualite/pas-decran-avant-trois-ans#:~:text=La%20commission%20écrans%20a%20rendu,adultes%20vers%20de%20bonnes%20pratiques.)). Face à cette situation, l’IRI a créé dès novembre 2017 la Clinique Contributive, **un dispositif expérimental** mis en œuvre avec l’équipe des professionnelles de santé de la **Protection Maternelle et Infantile** (**PMI**) Pierre Sémard, à Saint-Denis. Toutes les deux semaines, des chercheurs de diverses disciplines, des professionnels de santé et des parents se retrouvent pour **inventer ensemble des nouveaux savoirs sur les technologies numériques**, de plus en plus répandues dans l’éducation des enfants. Il s’agit d’essayer de répondre de façon innovante aux nouveaux problèmes qui en émergent.

Plusieurs projets sont menés avec l’équipe de la PMI. 

## Livret 

Nous avons travaillé en 2023 sur **un livret de sensibilisation à la question des écrans**, qui a été finalisé avec la mairie de Saint-Denis, des groupes de professionnels de santé, des parents, et des chercheurs. Il s’agissait de déconstruire certaines idées reçues les plus souvent entendues, comme : « l’écran aide l’enfant à se calmer pour qu’il arrête de pleurer ». Puis, le but était de proposer des explications appuyées scientifiquement, tout en renvoyant à **des activités alternatives aux écrans** existantes en Seine-Saint-Denis. Vous trouverez le livret [ici]( https://www.calameo.com/ville-saint-denis/read/0003435242149a8b77e0a?page=9). 

![livret écrans](/images/photos/activities/pmi_text1.png)

## Atelier jardinage 

Nous avons entamé au printemps 2024 un atelier de jardin contributif, qui allie la culture du jardin des locaux de la PMI, et la découverte de connaissances théoriques en botanique, afin de sentir et d’**expérimenter ce que cultiver veut dire**. La culture, dans son sens le plus large, demande du temps, de l’investissement, et des pratiques. Jardiner, cultiver, inventer un espace de cultures, découvrir les espèces de plantes, comme les plantes aromatiques que nous avons plantées, les effets du vent, du soleil, tout cela relève d’une culture de soi aussi, car **on s’y transforme soi-même en transformant ce qu’on a entre les mains**. A la rentrée 2024, nous allons continuer de planter des espèces nouvelles, d’observer les fleurs qui ont poussé spontanément dans le jardin laissé en repos, et découvrir le dessin botanique.

Depuis la rentrée 2024, nous explorons différentes espèces de plantes, nous observons les fleurs qui ont poussé spontanément, le jardin en repos pour l’hiver, les plantes aromatiques que nous avons plantées récemment, et nous découvrons le dessin botanique.

![jardinage](/images/photos/activities/pmi_text2.jpeg)

## Écriture

En 2024-25, le groupe de la PMI entamera un projet d’écriture à long-terme dans l’objectif de documenter les savoirs constitués depuis le début du projet. Nous travaillerons sur les **jeux de société et autres jeux** de médiation existants sur la question des écrans, afin d’écrire une critique, un commentaire, qui seront le point de départ de ce travail d’écriture collectif, pour se tester, pour se construire en tant que groupe d’écriture pour peut-être aller plus loin ensuite. 


## Séminaire théorique

En parallèle de toutes ces activités, un séminaire théorique a lieu tous les deux mois, où des chercheurs de différentes disciplines sont invités pour approfondir les aspects théoriques liés à la surexposition aux écrans et qui nous ont permis de couvrir des sujets tels que le **développement du langage**, la relation entre parent et enfant, **l’addiction**, **la disruption**, l’autisme, **l’économie de l’attention**, le rapport aux images, la psychomotricité et le corps…

![ecriture](/images/photos/activities/pmi_text3.jpg)
