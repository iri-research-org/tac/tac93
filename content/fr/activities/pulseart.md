---
title: "Pulse-Art"
description: "Expérimenter et évaluer l’impact des connaissances et savoirs culturels dans l’éducation"
date: 2024-11-20
weight: 5
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Pulse-Art"
#     weight: 6
#     parent: "activities"
#     params:
#       description: "Expérimenter et évaluer l’impact des connaissances et savoirs culturels dans l’éducation"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/pulseart.jpg"
icon: ""

images: []

hero:
  headings:
    heading: "Pulse-Art"
    sub_heading: "Expérimenter et évaluer l’impact des connaissances et savoirs culturels dans l’éducation"
  background:
    background_image: "images/photos/pulseart/pulse-art-01-1024.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2025-
  text: "Ce projet s’attache à comprendre et analyser dans 7 études de cas les connaissances et savoirs culturels (Cultural Awareness and Expression) et leur rôle dans l’éducation et la capacitation (dans et hors de l’école)."
  link_label: 
  link_url: ""
  partners: 
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Science for Language"
      link: https://scienceforchange.eu/
    - name: "Barcelona lnstitute for Health"
      link: https://www.isglobal.org/
    - name: "WAAG Future Lab"
      link: https://waag.org/
    - name: "Anatolia College"
      link: https://anatolia.edu.gr/en/about/institutional-description
    - name: "Foundation for Research and Technology"
      link: https://www.forth.gr/en/home/
    - name: "Museo Nacional de Ciencias Naturales"
      link: https://www.mncn.csic.es/en/quienes_somos/presentacion
    - name: "University of Malta"
      link: https://www.um.edu.mt/
    - name: "Euro-mediterranean Economists Association"
      link: https://euromed-economists.org/
    - name: "Daugavpils University"
      link: https://du.lv/
  fundings: 
    - name: "Union européenne"
      link: https://erasmus-plus.ec.europa.eu/

---


PULSE-ART v
se à comprendre et favoriser le **développement des connaissances et savoirs culturels dans l’éducation et la capacitation de la jeunesse européenne** (Cultural Awareness and Expression, CAE). Le projet intègre une perspective d’apprentissage tout au long de la vie en identifiant les lacunes et en surmontant les obstacles afin de favoriser les opportunités d’accroître les capacités du système éducatif à soutenir les apprenants dans l’acquisition de connaissances et la pratique de savoirs. Le projet implique divers établissements d’enseignement, des organisations artistiques et du patrimoine culturel et des centres d’innovation dans des partenariats collaboratifs pour produire des preuves de concept et éclairer les politiques.

Les 7 études de cas ciblées :

1. Game jams (Université de Malte, MA)
2. Jeux vidéo (IRI, FR)
3. Arts de la scène (Waag, NL)
4. Illustration scientifique (Musée national des sciences naturelles, ES)
5. Danse (Université de Daugavpils, LV)
6. Art visuel numérique (Fondation pour la recherche et la technologie Hellas, GR)
7. Musique (Association Euro Méditerranéenne des Economistes, MR)

![kick off workshop](/images/photos/activities/pulseart_text1.jpg)
