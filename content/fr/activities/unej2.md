---
title: "Urbanités numériques en jeux #2"
description: "Connaissances, savoirs et créativité dans des projets de concertation urbaine"
date: 2024-11-20
weight: 5
portfolio: ["research"]
partenaires: ["L’institut de recherche et d’innovation", "le Cabinet d’architecture et d’urbanisme O’zone", "ICI !", "Initiatives construites Îlo-Dionysiennes (association d’architectes)", "l’Association 3 Hit Combo, porteuse du projet RennesCraft", "Le Rectorat de l’Académie de Créteil", "le CDOS 93 (Comité Départemental Olympique et Sportif de Seine-Saint-Denis)"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Urbanités numériques en jeux #2"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Connaissances, savoirs et créativité dans des projets de concertation urbaine"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/unej2.JPG"
icon: ""

hero:
  headings:
    heading: "Urbanités numériques en jeux #2"
    sub_heading: "Connaissances, savoirs et créativité dans des projets de concertation urbaine"
  background:
    background_image: "images/photos/activities/unej2_background.JPG"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2025-
  text: "La dynamique créée avec les établissements scolaires s’étend au cadre de la concertation urbaine en collaboration avec architectes, artistes, fabLabs et collectivités."
  link_label: 
  link_url: ""

---


Fort de l’expérience du projet UNEJ, l’IRI a relancé une dynamique de travail autour d’un projet Urbanités Numériques en Jeux dans une nouvelle phase.

Cette deuxième phase a pour objectif de sensibiliser les jeunes habitants d'un territoire à la fabrique de la ville, et les impliquer dans la **concertation urbaine**. Cela implique le développement de savoirs théoriques, savoir-faire et savoir-vivre liés à l'urbanisme, à l'environnement, au genre, à l'insertion, à l'intergénérationnel et au développement d'un numérique responsable. L'enjeu est le pouvoir d'agir des habitants. Cela intervient dans le contexte de villes de plus en plus connectées qui récoltent des données sur leurs habitants sans leur ouvrir une possibilité d'agir et d'avoir son rôle à jouer, ni même de comprendre comment la ville se fait, de l'**aménagement du territoire**, en passant par des questions de ressources, d'architecture ou d'urbanisme. 

A travers un dispositif numérique qui met en place **le jeu Minetest** (équivalent libre et gratuit de Minecraft, jeu parmi les jeux les plus joués au monde), l'idée est de pouvoir mobiliser un public d'habitants jeune, difficilement mobilisable avec les outils de concertation classiques. Ce dispositif constitue, par l'accointance des jeunes avec le numérique et encore plus ce type de jeux dits "bac à sable", où l'on peut construire son monde, une solution à l'éloignement des jeunes d'une forme de citoyenneté, de droit à la ville, et de fabriquer la ville ensemble. 

*Méthode* : 
Utilisation de Minetest en lien avec les données géographiques de l'IGN (Institut National de l'Information Géographique et Forestière) comme outil de modélisation de la ville (maquette numérique simplifiée du territoire) 
- Mise en place de dispositifs de travail en groupe à travers des **ateliers de modélisation**, des balades urbaines... (approche par projets), chaque groupe a à débattre et présenter son projet, ce qui permet le déploiement des idées dans le projet 
- Expérimentation, principe d'essai-erreur, on essaye, on peut se tromper, on recommence, on trouve... 
- **Valorisation du territoire**, relecture des atlas, reconvocation de l'**imaginaire urbain**, régénération du regard que les jeunes habitants posent sur leurs lieux de vie, et renouvellement de leurs imaginaires (en passant notamment par les mangas...) 

