---
title: "Association En veille"
description: "Parents ensemble contre la surexposition aux écrans"
date: 2024-11-20
weight: 1
portfolio: ["research"]
header_transparent: true

# menu:
#   main:
#     name: "Association En veille"
#     weight: 5
#     parent: "activities"
#     params:
#       description: "Parents ensemble contre la surexposition aux écrans"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/parentsa.jpg"
icon: ""
axe: parentalite-soin-numerique

hero:
  headings:
    heading: "Association En veille"
    sub_heading: "Parents ensemble contre la surexposition aux écrans"
  background:
    background_image: "images/photos/activities/parentsa_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  period: 2024-
  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
  text: "Cette association de parents soutenue par l’IRI et la CAF93 travaille sur des modalités de partage d’expérience et de transmission auprès des collectivités, professionnels et parents confrontés à la problématique des écrans."
  link_label:
  link_url:
  fundngs:
    - name: "CAF93"
      link: https://www.caf.fr/allocataires/caf-de-la-seine-saint-denis
---

La Clinique Contributive et la formation “Raisonnons nos écrans” nous ont emmenés au fil des années à rencontrer un certain nombre de parents motivés. Ce groupe de parents a souhaité se fédérer, et se constituer en association. Les parents qui la composent ont parfois surexposé leurs enfants aux écrans, ont souvent pris conscience des dangers et ont trouvé de nouvelles pratiques évitant cette surexposition. Ils ont développé́ un **savoir-faire pour encourager leurs enfants à sortir des écrans**, proposer des activités et se réintroduire comme partenaires de la relation. Leur témoignage vaut tous les encouragements auprès des parents en difficulté́, et auprès des professionnels qui peuvent parfois être démunis pour parler de cette problématique avec les parents sans les culpabiliser. 
L’association a été créée au printemps 2024 et s’appelle « En Veille. Reconnectons-nous les uns aux autres ». 

![réunion en veille](/images/photos/activities/parentsa_text1.jpeg)


## Les trois grands objectifs de l'association

- **Sensibiliser d’autres familles**, professionnels, habitants, au sujet de la surexposition aux écrans et de ses dangers pour l’enfant.
- Proposer des activités alternatives aux écrans sur le territoire de la Seine-Saint-Denis.
- Perpétuer le lien avec le monde de la recherche, continuer de se former, d’apprendre, d’organiser des séminaires sur le sujet.

## La question financière

Il s’agit aussi pour l’IRI de tenter de réfléchir à la question des **conditions financières de participation des parents**. Cette réflexion a lieu à travers le modèle de l’économie contributive avec un revenu contributif auquel les parents auraient droit à priori, renouvelable à condition de valoriser les savoirs produits et acquis dans le cadre d’emplois contributifs intermittents.  Nous travaillons sur ce modèle, et plusieurs questions se posent : comment formaliser et transmettre ses connaissances ? Comment gérer les modalités financières d’intervention et la rémunération dans le groupe ? Quel fond potentiel pour soutenir les actions des parents ? Cette réflexion est toujours actuelle.


Avec l’appui de l’institut de recherche et d’innovation. Soutenu par la CAF93. 



