---
title: "Urbanités numériques en jeux #1"
description: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec les établissements scolaires de la Seine Saint-Denis."
period: 2019-2024
date: 2024-11-20
weight: 1
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "Urbanités numériques en jeux #1"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec les établissements scolaires de la Seine Saint-Denis."

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/unej.png"
icon: ""

hero:
  headings:
    heading: "Urbanités numériques en jeux #1"
    sub_heading: "De la cour d’école au village olympique, construire un nouveau génie urbain et des parcours pédagogiques avec des établissements scolaires de la Seine Saint-Denis."
  background:
    background_image: "images/photos/activities/unej_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content:
  partners: 
    - name: "Institut de Recherche et d’Innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Cabinet d’architecture et d’urbanisme O’zone"
      link: http://www.ozone-architectures.com/
    - name: "ICI !, Initiatives Construites Îlo-Dionysiennes (association d’architectes)"
      link: https://www.associationici.com/
    - name: "Association 3 Hit Combo, porteuse du projet RennesCraft"
      link: https://www.3hitcombo.fr/rennescraft/
    - name: "DANE de l’Académie de Créteil"
      link: https://dane.ac-creteil.fr/
    - name: "IGN"
      link: https://www.ign.fr/
    - name: "CDOS 93 (Comité Départemental Olympique et Sportif de Seine-Saint-Denis)"
      link: https://www.cdos93.org/
    - name: "Solidéo"
      link: https://www.ouvrages-olympiques.fr/
  text: "UNEJ vise à capaciter des enseignant.e.s et leurs élèves pour faire des propositions d’aménagement de leur établissement et de leur quartier. 12 établissements scolaires de Seine-Saint-Denis inventent le futur de leur ville et du Village Olympique hérité en s’appuyant sur une version libre et open source de Minecraft."
  link_url: "https://unej.tac93.fr/"
  fundings: 
    - name: "Caisse des Dépôts et Consignation"
      link: https://www.caissedesdepots.fr/
    - name: "Ministère de l’Éducation Nationale"
      link: https://www.education.gouv.fr/
    - name: "Académie de Créteil"
      link: https://ac-creteil.fr/
    - name: "Fondation de France"
      link: https://www.fondationdefrance.org/fr/
    - name: "Fondation SNCF"
      link: https://www.groupe-sncf.com/fr/engagements/mecenat-sponsoring/fondation
    - name: Conseil Départemental de la Seine Saint-Denis
      link: https://seinesaintdenis.fr/
    - name: Fondation NextInternet
      link: https://nlnet.nl/project/MinetestEdu/
---

Le projet **Urbanités Numériques En Jeux (UNEJ)** est une expérimentation menée de septembre 2020 à décembre 2024 dans le cadre du programme Territoire Apprenant Contributif. Il est le fruit d’un partenariat entre l’Institut de Recherche et d’Innovation (IRI) et la Délégation Académique au Numérique Éducatif (DANE) de l’Académie de Créteil pour son pilotage, ainsi que le conseil départemental de la Seine-Saint-Denis pour sa gestion et sa mise en œuvre.

![élèves sur Minetest](/images/photos/unej/UNEJ-page003.jpeg)

Pendant quatre années, le projet s’est concentré sur l’organisation d’ateliers réalisés sur le temps scolaire, animés par des enseignant·e·s volontaires. Ces ateliers ont reposé sur un dispositif incluant le jeu vidéo [Luanti](https://luanti.fr) (équivalent libre de Minecraft, anciennement Minetest) pour accompagner des élèves de 8 collèges et 2 lycées dans la conception et la modélisation de propositions d’aménagements urbains en lien avec leur territoire. Tous ces travaux se sont inscrits dans la dynamique des Jeux Olympiques et Paralympiques de Paris 2024.
Cette initiative a mobilisé une collaboration multi-acteurs et une démarche de co-construction inspirée des principes de « recherche contributive » développés à l’IRI. Les partenaires professionnels ont accompagné les enseignant·e·s dans le développement des connaissances et des savoirs nécessaires à l’organisation des ateliers. Parmi eux figuraient l’IGN, le CDOS 93, l’association 3 Hit Combo (représentée par Thomas François, aujourd’hui indépendant), le cabinet d’architecture O’Zone, ainsi que l’association d’architectes ICI!. Ces acteurs ont joué un rôle majeur dans la réussite et l’essaimage du dispositif.

## Historique

La proposition initiale du dispositif Urbanités Numériques En Jeux s’est formée entre 2018 et 2019, à partir des travaux de recherche développés par Bernard Stiegler et l’équipe de l’Institut de Recherche et d’Innovation, notamment lors des Entretiens du Nouveau Monde Industriel de 2018, intitulés [L’intelligence des villes et la nouvelle révolution urbaine](https://enmi-conf.org/wp/enmi18/). Introduit par Patrick Braouezec (alors président de l’EPT Plaine Commune) et Bernard Stiegler, cet événement interrogeait le devenir industriel de la ville à l’époque des plateformes et des nouvelles technologies du bâtiment, comme le BIM (Building Information Modelling).

{{< framework/video url="https://media.iri.centrepompidou.fr/video/unej/expo%20biennale%20saint-etienne%202022/EXTRAITS%20VID%C3%89O%20-%20Saint-Etienne%20-%20SUB.mp4">}} 

De nombreuses expériences urbaines y ont été présentées, notamment [Rennescraft](https://www.3hitcombo.fr/rennes-craft/), qui utilisait Minecraft et les données du SIG de la ville de Rennes. Ces entretiens ont abouti à la publication d’un ouvrage collectif, *[Le nouveau génie urbain](https://boutique.fypeditions.com/products/le-nouveau-genie-urbain?srsltid=AfmBOoqaHLoe0TunMLeUsxUYjvxLWR0S6-9jBnH1qLzYUfV_fFU2kbtf)*, qui a fourni un cadre théorique clair pour les échanges entre partenaires et l’organisation des ateliers. Cette base a permis d’élaborer une approche pédagogique adaptée à l’enseignement, en impliquant des enseignant·e·s de Seine-Saint-Denis dès les premières étapes.

## Méthodologie

Le dispositif s’est appuyé sur des apports pédagogiques variés pour traiter les problématiques d’aménagement. Les élèves ont progressivement élargi leur champ d’action : après avoir travaillé sur leur cour de récréation, ils se sont intéressés à leur quartier pour finalement se pencher sur le village Olympique et Paralympique.

Les disciplines les plus sollicitées ont été les Mathématiques, la Technologie et l’Histoire-Géographie, souvent complétées par la SVT, la Physique-Chimie et le Français selon les thématiques abordées, comme l’écologie, la gestion de l’eau ou l’élaboration d’un argumentaire pour présenter ses idées.

![méthode unej](/images/photos/unej/unej_methode.jpg)

L’usage de Luanti a favorisé la collaboration entre élèves, l’entraide, et le développement de différents savoirs. La méthodologie, élaborée par O’zone, ICI! et l’IRI, s’est articulée autour de trois phases distinctes :

1. Les élèves ont pris en main l’outil numérique, exploré numériquement le territoire et visité physiquement l’espace à aménager pour en identifier les enjeux.

2. Après avoir reconstruit le territoire sur Luanti à partir de plans d’aménagement fournis par les urbanistes, les élèves ont réfléchi collectivement aux problématiques à traiter — écologiques, sociales ou esthétiques. Cette réflexion a abouti à la modélisation de leurs propositions, qu’ils ont ensuite présentées devant un jury.

3. Enfin, ils ont modélisé leurs propositions, préparé une présentation et restitué leur travail devant un jury.

Cette méthodologie structurée, qui allie exploration, réflexion collective et modélisation, a été déclinée dans des formats variés pour s’adapter aux besoins et aux contraintes des établissements scolaires participants.

## Dispositif Technique

Luanti a été l’élément clé du dispositif grâce à sa capacité à être pris en main très rapidement et à ses fonctionnalités, comparables à une pratique numérique des legos. Cet outil libre et gratuit, équivalent de Minecraft – jusqu’à récemment considéré comme le jeu vidéo le plus téléchargé au monde –, est facilement accessible à de jeunes élèves, souvent déjà familiers avec ce type de jeux. Ce choix s’est imposé à la suite d’un travail de veille technologique collective et contributive, amorcé avec Thomas François, l’un des fondateurs du projet Rennescraft. Si Minecraft permet de mobiliser des connaissances acquises hors du cadre scolaire, son statut de logiciel payant et propriétaire, ainsi que les limites de sa version éducative, freinaient son usage en classe. À l’inverse, Luanti, libre et modulaire, répondait bien mieux aux attentes pédagogiques par sa gratuité, sa flexibilité et sa faible consommation en ressources informatiques.

{{< framework/video url="https://media.iri.centrepompidou.fr/video/unej/expo%20biennale%20saint-etienne%202022/ITW%20POINCAR%C3%89%20-%20Saint-Etienne%20-%20SUB.mp4">}} 

Dans le cadre du projet UNEJ, les données géographiques ont été transformées en cartes de voxels 3D grâce au service [« Minecraft à la carte »](https://minecraft.ign.fr/) de l’IGN, permettant de représenter le territoire de la Seine-Saint-Denis. En partenariat avec l’IGN, une carte couvrant tout le département a été générée, puis hébergée sur un serveur maintenu par l’IRI. Chaque établissement scolaire participant disposait d’un accès privé à une zone dédiée de cette carte. En complément, une version en deux dimensions de la carte était mise à disposition pour faciliter la localisation des zones et la préparation des activités. Par ailleurs, l’IRI a fourni des mods adaptés pour aider les enseignant.e.s à naviguer dans la carte, gérer les sessions de jeu et répondre aux besoins pédagogiques spécifiques de leurs élèves.

Pour en savoir plus sur le dispositif, vous pouvez voir ce [tutoriel détaillé](/doc/howto-unej/)

## Formation des enseignant.e.s

Depuis 2020/2021, trois journées de formation annuelles ont été organisées à l’initiative du rectorat de Créteil pour accompagner les enseignant.e.s dans le projet UNEJ. Ces formations visaient à renforcer leurs compétences théoriques et pratiques, à développer des séquences pédagogiques et à collaborer avec des architectes référents.

![formation d'enseignants](/images/photos/unej/unej_formation.JPG)

Première année : bases théoriques et découverte de Minetest
La première année a posé les fondations, avec des interventions sur les enjeux numériques des villes (Dominique Boullier, Vincent Puig) et les politiques d’aménagement sportif (CDOS93). Les enseignant.e.s ont exploré la carte voxel de la Seine-Saint-Denis et acquis les bases nécessaires pour intégrer Minetest dans leurs projets éducatifs.

Deuxième année : pratiques et projets concrets
Les formations se sont orientées vers des ateliers pratiques, comme la conception d’aménagements de cours de récréation, et des échanges d'expériences. Des spécialistes telle que Valérie Peugeot ont apporté des éclairages innovants, tandis que les enseignant.e.s ont approfondi leurs connaissances des outils numériques qui ont constitué le projet.

Années suivantes : perfectionnement et bilan
Les années suivantes ont approfondi les pratiques avec des focus sur la programmation (Python avec Minetest), les outils professionnels (maquette BIM du VOP), et l’évaluation des apprentissages. La dernière année a permis un bilan global des projets et a mis en avant la pérennisation des ressources éducatives libres et des contributions des enseignant.e.s au-delà du projet.

## Ateliers scolaires

Trois modes d’organisation ont été mis en place pour s’adapter aux contraintes des établissements. Certains ateliers ont pris la forme de séances bi-hebdomadaires de deux heures avec les mêmes classes tout au long du projet. D’autres ont fonctionné sous forme de clubs hors temps scolaire, ouverts sur inscription et souvent reconduits d’une année sur l’autre. Enfin, des ateliers intensifs ont été organisés sur deux ou trois jours lors de semaines dédiées en fin de trimestre.

Les architectes et enseignant.e.s ont travaillé en étroite collaboration pour optimiser le déroulement des ateliers. Ils ont conclu qu’il était plus pertinent de viser des objectifs annuels reconductibles, plutôt qu’un planning pluriannuel.

![balade urbaine](/images/photos/unej/UNEJ-page004.jpeg)

### Projets thématiques

La cour de récréation

Chaque établissement a travaillé sur sa cour en utilisant les méthodes proposées par les architectes référents. Les élèves ont mené des visites, enquêtes et observations avant de formuler des propositions d’aménagement inspirées des projets de cour « Oasis », visant à rendre ces espaces plus accueillants et adaptés.

Le quartier proche

En collaboration avec les collectivités locales, les élèves ont exploré leur environnement immédiat. Ils ont participé à des concertations urbaines, rencontré des élus, visité des espaces publics et intégré ces expériences dans leurs modélisations.

Le village olympique et paralympique

Certains établissements ont orienté leurs travaux sur des espaces liés à l’héritage des Jeux Olympiques. Le collège Dora Maar, situé près du Village Olympique, s’est concentré sur l’aménagement du mail Finot. Par ailleurs, un atelier de programmation Python a permis à des élèves de développer des outils pour enrichir leurs modélisations.

![atelier village olympique](/images/photos/unej/UNEJ-page006.jpeg)

Modélisation collective

Lors de la deuxième année, un atelier collaboratif a réuni plusieurs établissements autour du thème de l’eau. Guidés par des architectes, les élèves ont imaginé ensemble des aménagements pour les bords de Seine, intégrant des enjeux d’écologie et d’urbanisme.

![modélisation village aquatique](/images/photos/unej/UNEJ-page014.jpeg)

Restitution finale

Une restitution a été organisée à la serre Wangari pour clôturer ces quatre années de travail. Les élèves ont finalisé leurs projets avant de les présenter aux Archives Nationales, mettant en lumière leurs idées et réalisations.

![restitution finale](/images/photos/unej/UNEJ-page008.jpeg)

### Galerie des projets

Voici une sélection des projets réalisés par les élèves sur Luanti pendant les quatre années

{{< gallery Match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview=true loadJQuery=true >}}