---
title: "Séminaire monnaie"
description: "Un espace d’échange et de recherche sur l’économie contributive et le rôle des monnaies locales"
date: 2024-11-20
weight: 6
portfolio: ["research"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Séminaire monnaie"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Un espace d’échange et de recherche sur l’économie contributive et le rôle des monnaies locales"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/seminaire-monnaie.png"
icon: ""

hero:
  headings:
    heading: "Séminaire monnaie"
    sub_heading: "Un espace d’échange et de recherche sur l’économie contributive et le rôle des monnaies locales"
  background:
    background_image: ""
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2016-
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Université de Bordeaux"
    link: https://www.u-bordeaux.fr/
  text: "Ce séminaire soutenu par la Caisse des dépôts et la Fondation de France propose chaque année de dialoguer avec des porteurs de projets d’initiatives économiques pouvant prolonger ou infléchir les principes de l’économie contributive : monnaies locales, modèles comptables, modèles coopératifs, propositions monétaires, nouveaux cadres d’investissement"
  link_label: Enregistrement du séminaire monnaie 2024
  link_url: "https://iri-ressources.org/collections/season-72.html"
  fundings: 
  - name: "Caisse des dépôts"
    Link: https://www.caissedesdepots.fr/
  - name: "Fondation de France"
    link: https://www.fondationdefrance.org/fr/
---

Déscription à venir.