---
title: "Activities"
description: "Une sélection des activités liées au programme TAC"
date: 2018-02-10T11:52:18+07:00

header_transparent: true

hero:
  headings:
    heading: "Activités"
    sub_heading: "Une sélection des activités liées au programme TAC"
  background:
    background_image: "images/photos/horizontal-front-image.png"
    background_image_blend_mode: "overlay"
    background_gradient: true
---

