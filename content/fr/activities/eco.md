---
title: "Comité Eco"
description: "Relier habitants et acteurs économiques, soutenir les initiatives écologiques et sociales à travers une nouvelle monnaie locale"
date: 2024-11-20
weight: 3
portfolio: ["research"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Comité Eco"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Relier habitants et acteurs économiques, soutenir les initiatives écologiques et sociales à travers une nouvelle monnaie locale"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/eco.png"
icon: ""

hero:
  headings:
    heading: "Comité Eco"
    sub_heading: "Relier habitants et acteurs économiques, soutenir les initiatives écologiques et sociales à travers une nouvelle monnaie locale"
  background:
    background_image: "images/photos/activities/eco_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2022-
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Plaine commune"
    link: https://plainecommune.fr/
  - name: "Odyssée"
    link: http://odyssee.co/fr/identity.html
  - name: "La Régie"
    link: https://www.facebook.com/RegiedeQuartiersdeSaintDenis/?locale=fr_FR
  - name: "PHARE"
    link: http://www.lephares.coop/
  - name: "Mains d’ouvres"
    link: https://www.mainsdoeuvres.org/
  text: "La mise en place d’une nouvelle monnaie territoriale pour favoriser de nouveaux liens entre habitants, associations, entreprises et sociétés et valoriser les actions écologiques et socialement vertueuses. Portée par l’agence d’urbanisme Odysée et soutenue par l’institut de recherche et d’innovation."
  link_label: Site web
  link_url: "https://www.comite-eco.fr/comite-eco/"
  fundings: 
  - name: "Société de livraison des ouvrages olympiques"
    link: https://www.ouvrages-olympiques.fr/
  - name: "Fondation de france"
    link: https://www.fondationdefrance.org/fr
  - name: "Fondation Macif"
    link: https://www.fondation-macif.org/
---

De multiples travaux d’Ars Industrialis et de l’IRI élaborent autour de la question du revenu contributif, des manières de compter, d’un point de vue comptable. Ces travaux restent très théoriques pendant de nombreuses années. En 2022, une forte opportunité émerge à la suite d’un appel à projet de la SOLIDEO « Smart Citizen » : comment faire en sorte de favoriser les comportements des habitants vers la **transition écologique** ? Le comité ÉCO est une association créée en 2023 pour répondre à cet appel à projet, sur le territoire de Plaine Commune, avec pour média le développement d'une monnaie locale : l’ÉCO, qui viendrait valoriser les acteurs locaux sur le territoire. Il réunit l’IRI et Odyssée, agence d’urbanisme. Le projet vise à accompagner les habitants à **reprendre la main sur leur alimentation, leur cadre de vie, leur mobilité, leur consommation, et la biodiversité** des milieux dans lesquels ils vivent à travers l’utilisation de cette monnaie et de l’application qui va l’accompagner. L’ÉCO se veut être l’outil qui accompagne l’engagement des personnes et propose aussi des rencontres et échanges pour favoriser le lien social sur le territoire.

![eco app](/images/photos/activities/eco_text1.png)

Le projet comporte des objectifs précis : **circulation de la monnaie**, et notamment pendant les Jeux Olympiques 2024, chez les commerçants locaux, favoriser ainsi des « **parcours de vie** », c’est-à-dire des actions qui vont permettre d’utiliser l’ÉCO**. De plus, utiliser l’ÉCO va impliquer la constitution d’une réserve (3% des échanges) dont pourra disposer le collectif, cela doit être encore grandement réfléchi et discuté.

Afin de définir les actions qui rentreraient potentiellement dans le cadre de ce que la monnaie souhaite valoriser pour le territoire, **un comité scientifique** va être constitué, avec des chercheurs, des habitants, des élus, afin de réfléchir et de décider quelles actions ont un potentiel pour le territoire sur les volets cités, en termes de soutenabilité. Un fort enjeu se situe sur cet aspect, et doit être considéré avec sérieux et soin. Des travaux de recherche sont en cours et notamment en économie : qu’est-ce qui génère de la motivation pour faire des choses ? Dans quelle mesure cette monnaie pourrait permettre de générer de la motivation pour se tourner vers un certain type d’action ? Comment faire en sorte que tous ces actes motivent des financements ? Le projet est une tentative pratique de **s’intéresser à l’économie du territoire**, mais implique le besoin d’analyses et de critiques théoriques sérieuses.
