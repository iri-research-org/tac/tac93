---
title: "ContribAlim"
description: "Créat
on contributive de recettes pour une cuisine collective"
period: 2022-2024
date: 2024-11-20
weight: 5
portfolio: ["research"]
axe: economie-de-la-contribution

header_transparent: false

# menu:
#   main:
#     name: "ContribAlim"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Création contributive de recettes pour une cuisine collective"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/contribalim.png"
icon: ""

hero:
  headings:
    heading: "ContrbAlim"
    sub_heading: "Création contributive de recettes pour une cuisine collective"
  background:
    background_image: "images/photos/activities/contribalim_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  period: 2022-2024
  client_label:
  client_content: 
  partners:
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Open Food Fact"
    link: https://world.openfoodfacts.org/
  - name: "Mosaic"
    link: https://mosaic.mnhn.fr/un-nouvel-outil/
  - name: "Université Sorbonne Paris Nord"
    link: https://www.univ-spn.fr/
  - name: "La ville de Saint Denis"
    link: https://ville-saint-denis.fr/
  - name: "Appui"
    link: https://appuii.wordpress.com/
  - name: "Ouishare"
    link: 
  - name: "Crisalim"
    link: https://www.crisalim.co/
  text: "ContribAlim est un dispositif de science participative et de recherche contributive pour le développement des savoirs alimentaires, déployé en Seine Saint-Denis et financé par l’ADEME."
  link_label: Application web
  link_url: "https://contribalim.mnhn.fr/"
  fundings: 
  - name: "ADEME"
    link: https://www.ademe.fr/en/frontpage/
---


Comment s’appuyer sur les référentiels alimentaires et sur les connaissances scientifiques pour soutenir un processus de capacitation sur un territoire exposé à de multiples formes de précarité alimentaire ? Mieux s’approvisionner, mieux cuisiner, mieux partager un repas, **mieux percevoir et cultiver les saveurs, mieux lutter contre les pathologies alimentaires**, autour de quelle résolution de problème ou autour de quel désir ces savoirs sont-ils construits ? Sous l’impulsion de l’Institut de Recherche et d’Innovation (IRI), et grâce à un financement de l’ADEME dans le cadre du dispositif CO3, le projet ContribAlim a réuni de 2022 à 2024 des équipes de recherche (Mosaic, LEPS, EREN), des acteurs du territoire de Saint Denis (APPUI, Crisalim), le service santé de la Ville et la société OpenFoodFacts pour co-concevoir un dispositif de contribution par des recettes modifiables selon leur **Nutriscore**.

![screenshot du site de web](/images/photos/activities/contribalim_text1.png)