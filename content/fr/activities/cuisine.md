---
title: "Cuisine contributive"
description: "Cuisiner ensemble des plats en grande quantité"
date: 2024-11-20
weight: 5
portfolio: ["research"]
partenaires: ["Institut de recherche et d’innovation", "Taf et Maffé", "La Petite Casa"]
axe: economie-de-la-contribution
header_transparent: true

# menu:
#   main:
#     name: "Cuisine contributive"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Cuisiner ensemble des plats en grande quantité"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/cuisinecontributive.png"
icon: ""

hero:
  headings:
    heading: "Cuisine contributive"
    sub_heading: "Cuisiner ensemble des plats en grande quantité"
    period: 2022-2024
  background:
    background_image: "images/photos/activities/cuisinecontributive_background.png"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content:
  period: 2022-2024
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Taf et Maffé"
    link: https://plainecommune.fr/
  - name: "La Petite Casa"
    link: http://odyssee.co/fr/identity.html
  text: "Ce dispositif intégré aux parcours ECO vise à développer des pratiques écologiques vertueuses, à capaciter des personnes éloignées de l’emploi et à soutenir la valorisation de leurs savoirs dans de nouvelles activités économiques."
  link_label: 
  link_url: ""
  fundings: 
---


La cuisine contributive s’inscrit dans une dynamique de **démocratie alimentaire**, qui vise à créer les conditions concrètes pour que les habitants puissent choisir leur alimentation de manière éclairée, tout en respectant leur dignité, leur histoire et leur culture. En ce sens, il s’agit de travailler prioritairement avec des personnes qui font l’expérience de difficultés en lien avec l’alimentation (petits budgets, recours à l’aide alimentaire, pathologies alimentaires, isolement et difficulté à s’organiser etc.) ; et de recourir à une approche non stigmatisante, qui s’appuie sur leurs expériences, leurs désirs et favorise la prise en charge progressive par les participants des différentes étapes du dispositif.  

Ce projet s’inspire d’une pratique québécoise, les **cuisines collectives**, reposant sur le **regroupement de petits groupes d’habitants** qui mettent en commun leur temps, leur argent et leurs savoirs, pour cuisiner ensemble des plats économiques, sains et appétissants à ramener à la maison. En ce sens, il s’agit dans notre cas de proposer à un groupe (8 à 10 personnes) composé de salariés en insertion et d’étudiants de cuisiner ensemble tous les mois des **plats que chacun ramènera chez lui**.

Le déroulé de l’atelier se fait à chaque fois selon 4 phases :

1. planification : définition collective des recettes à cuisiner et du nombre de portions que chacun souhaite des différents plats, établissement du budget
2. achats
3. cuisine
4. évaluation du dispositif et des plats

Pour permettre de mener ces différentes phases de manière constructive et conviviale, chaque session se décompose en deux demi-journées qui ont lieu au sein du restaurant social Taf et Maffé.

![cuisine1](/images/photos/activities/cuisinecontributive_text1.png)


Ce projet poursuit deux objectifs :

- Apporter **une réponse capacitante à la problématique de la précarité alimentaire** : il s’agit de promouvoir une approche visant à aborder de manière transversale les multiples enjeux liés à cette situation sociale, à la fois économiques – mutualiser les achats pour abaisser les coûts – sanitaires – travailler à l’équilibre nutritionnel des plats – mais aussi sociaux – créer du lien entre des publics qui autrement ne se rencontreraient pas nécessairement – et culturels – **faire une place à la culture alimentaire des participants** qui sont les prescripteurs des recettes qui seront cuisinées.
- Constituer un « laboratoire » où, à partir des savoirs développés collectivement, pourront s’inventer de manière patiente de nouvelles activités économiques, culturelles ou sociales du territoire** : traiteur, cantine de quartier, conserverie artisanale, etc.
