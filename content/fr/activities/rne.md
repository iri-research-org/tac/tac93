---
title: "Raisonnons nos écrans"
description: "Parents, chercheurs professionnels pour se former et se capaciter à des pratiques raisonnées des écrans"
period: 2022-2025
date: 2024-11-20
weight: 4
portfolio: ["research"]
partenaires: ["Institut de recherche et d’innovation", "Ville de Saint Denis", "Mildeca"]
axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Raisonnons nos écrans"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Parents, chercheurs professionnels pour se former et se capaciter à des pratiques raisonnées des écrans"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/rne.jpg"
icon: ""

hero:
  headings:
    heading: "Raisonnons nos écrans"
    sub_heading: "Parents, chercheurs professionnels pour se former et se capaciter à des pratiques raisonnées des écrans"
  background:
    background_image: "images/photos/blur/rne_background.jpg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 
  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Ville de Saint Denis"
      link: https://ville-saint-denis.fr/
  text: "Six formations de 9 séances chacune, proposées avec la Ville de Saint-Denis et suivies depuis 2021 par 60 professionnels et 30 parents pour collectivement inventer des stratégies contre la surexposition aux écrans de tout-petits enfants. Programme soutenu par la Mildeca (Mission interministérielle de lutte contre les conduites addictives)."
  link_label: 
  link_url: 
  fundings:
    - name: "La Mildeca"
      link: https://www.drogues.gouv.fr/
---

Depuis 2022 et pour trois ans, l’activité de la Clinique Contributive s’est étendue à un projet de formations intitulées « Raisonnons nos écrans » pour les parents et les professionnels de santé, dans le cadre d’**un partenariat avec la Mission interministérielle de lutte contre les drogues et les conduites addictives et la Mairie de Saint-Denis**. Ces formations ont lieu à Saint-Denis et alternent entre maisons de quartier, médiathèques, centre sociaux.

![rne séance](/images/photos/activities/rne_text1.jpg)

Ces formations durent 9 séances et touchent une centaine de personnes au total. Elles ont lieu toutes les deux semaines sur une demi-journée, et s'inspirent de la méthode de la Clinique Contributive. Les chercheurs, parents, professionnels de santé réfléchissent ensemble au problème de la **surexposition aux écrans** et tentent de créer des nouveaux savoirs pour y faire face. 

Les ateliers mettent en œuvre :
- L’analyse individuelle et collective des pratiques numériques des participants, en partant du principe que tout le monde, quel que soit son statut, est impliqué dans le problème de la surexposition aux écrans, et que chacun a quelque chose à apporter à la **réflexion collective**.
- Des apports théoriques sur le fonctionnement du cerveau, sur les vulnérabilités psychiques, sur **le développement de l’enfant** en bas âge et ses besoins, sur le fonctionnement des applications numériques et de l’économie de l’attention, sur l’aspect thérapeutique du groupe, sur les questions d’addiction, de relation parent-enfant…
- Une réflexion collective et un partage des **pratiques de désintoxication numérique** et de pratiques raisonnées, comme un échange d’astuces à partager pour essayer de faire évoluer son rapport aux écrans.
- La construction de **nouvelles pratiques professionnelles** et d’actions à mener : quels projets les participants souhaitent-ils initier : des cafés de parents ? des interventions dans des écoles ? des jeux inventés pour sensibiliser sur leur lieu de travail ? des outils ?

