---
title: "Survivre au digital"
description: "Cultiver savoir et soin du numérique à l’échelle européenne"
period: 2022-2024
date: 2024-11-20
weight: 5
portfolio: ["research"]

axe: parentalite-soin-numerique
header_transparent: true

# menu:
#   main:
#     name: "Survivre au digital"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Cultiver savoir et soin du numérique à l’échelle européenne"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/surviving-digital.jpeg"
icon: ""

hero:
  headings:
    heading: "Survivre au digital"
    sub_heading: "Cultiver savoir et soin du numérique à l’échelle européenne"
  background:
    background_image: "images/photos/activities/surviving_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label: 
  client_content: 

  partners:
    - name: "Institut de recherche et d’innovation"
      link: https://www.iri.centrepompidou.fr/
    - name: "Le Laba"
      link: https://lelaba.eu/laba/
    - name: "Momentum"
      link: https://momentumconsulting.ie/
    - name: "Iasis"
      link: https://www.iasismed.eu/
    - name: "Materahub"
      link: https://www.materahub.com/
    - name: "La ville de Saint Denis"
      link: https://ville-saint-denis.fr/
    - name: "European E-learning Institute"
      link: https://www.euei.dk/
  text: ""
  link_label: Site du projet
  link_url: "https://survivingdigital.eu/"
  fundings: 
    - name: "Union européenne"
      link: https://erasmus-plus.ec.europa.eu/

---


Propulsé par l’agence le LABA, le projet Survivre au digital vise à formaliser l’expérience menée en Seine-Saint-Denis depuis 2019 afin de pouvoir la partager avec un plus grand public, et décrire comment mener des ateliers sur ces sujets.  Destiné aux parents et aux professionnels touchés par la surexposition et l’addiction aux écrans, le projet a mené à 4 grands résultats :  

- Une étude présentant les enjeux de l’addiction aux écrans.
- Une méthode pour la création de groupes d’entraides pour lutter contre l’addiction aux écrans, réalisée à partir d’expériences des partenaires. L’IRI a dirigé la rédaction d’un [guide méthodologique]( https://cloud.svc.iri-research.org/sites/clinique_contributive/sub/guide%20methodologique#page-toc-8) et de valorisation pour mettre en place des actions de terrain.  
- Deux cursus de formation certifiante dédiés aux professionnels de l’accompagnement des jeunes parents et familles. 
- Une boîte à outils composée de 10 vidéos pédagogiques à destination des adultes bénéficiaires comme support pour les groupes de parole, et des outils de formation et d’information à destination des publics adultes et des formateurs.

Le projet Survivre au Digital est constitué de spécialistes de la santé, du numérique, et de l’éducation pour adulte en Irlande, en Italie, en Grèce et en France. 

![atelier à athène](/images/photos/activities/surviving_text1.jpeg)


Nous avons eu la chance de participer à une rencontre en avril 2024 avec deux des mères de l’association En veille, Nadia (qui était présente lors d’une formation RNE et était très motivée pour poursuivre le travail) et Hakima (qui est présidente de l’association et suit le projet depuis ses débuts historiques). Elles ont pu présenter le travail de la Clinique Contributive, la méthode employée, et plusieurs grandes idées qui les ont marquées. Elles ont mené un atelier sur le modèle de ceux que nous animons dans les formations RNE. Les échanges entre pays ont été très riches, nous avons discuté longuement de la question de l’ennui, de la frustration, en anglais et en français, et nous sommes très fiers de ce moment !

![présentation à athène](/images/photos/activities/surviving_text2.jpeg)
