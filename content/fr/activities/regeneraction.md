---
title: "RegénérAction"
description: "Identifier le « Urban Community Builder » comme rôle professionnel dans la communauté"
date: 2024-11-20
weight: 6
portfolio: ["research"]
axe: design-de-la-contribution-urbaine
header_transparent: true

# menu:
#   main:
#     name: "RegénérAction"
#     weight: 1
#     parent: "activities"
#     params:
#       description: "Identifier le « Urban Community Builder » comme rôle professionnel dans la communauté"

fa_icon: ""
image: ""
thumbnail: "images/photos/thumbnails/regeneraction.jpeg"
icon: ""

hero:
  headings:
    heading: "RegénérAction"
    sub_heading: "Inventer le « Urban Community Builder » comme rôle professionnel dans la communauté"
  background:
    background_image: "images/photos/activities/regeneraction_background.jpeg"
    background_image_blend_mode: "overlay"
    background_gradient: true

aside:
  client_label:
  client_content: 
  period: 2023-2025
  partners: 
  - name: "Institut de recherche et d’innovation"
    link: https://www.iri.centrepompidou.fr/
  - name: "Urbani Separe"
    link: https://www.urbanisepare.org/
  - name: "Matera Hub"
    link: https://www.materahub.com/
  - name: "Innohub"
    link: https://www.innohub.uk/
  - name: "University of Évora"
    link: https://www.uevora.pt/en
  link_label: Site du projet
  link_url: "https://regeneractionproject.eu/"
  fundings: 
  - name: "Union européenne"
    link: https://erasmus-plus.ec.europa.eu/
  - name: "Surviving Digital"
    link_url: "https://survivingdigital.eu/"
---

Dans l’axe de recherche Design et technologies de la contribution urbaine, ce projet s’appuie sur des échanges d’expérience entre partenaires européens pour définir, documenter et expérimenter la figure du « Urban Community Builder » dans des ateliers de capacitation focalisés sur les questions de récupération, circuits courts, économie circulaire au service d’une nouvelle urbanité