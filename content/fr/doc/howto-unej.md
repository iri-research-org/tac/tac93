---
title: "Tutoriel : Reproduire le dispositif UNEJ sur Luanti"
---

# Tutoriel : Reproduire le dispositif UNEJ sur Luanti

Ce tutoriel a pour objectif de détailler la mise en place le dispositif technique utilisé dans le cadre du projet **Urbanités Numériques En Jeux (UNEJ)**. Il explique étape par étape comment installer et configurer un serveur, ajouter les éléments nécessaires à son bon fonctionnement, et gérer les permissions pour organiser un espace collaboratif adapté aux besoins des élèves et enseignant·e·s. Que vous soyez novice ou expérimenté, ce guide vous permettra de déployer un environnement adapté aux usages pédagogiques et à l'exploration du territoire en jeu.


## 1. Génération et installation de la carte IGN

### 1.1 Génération de la carte
Avant toute chose, il faut choisir une carte à partir de laquelle travailler en la générant sur le service de l'IGN : [Minecraft à la carte](https://minecraft.ign.fr/).

### 1.2 Installation de la carte
Une fois la carte générée, elle sera reçue par mail sous forme de fichier ZIP. Pour l'installer dans Luanti :
1. Dézipper le fichier dans le dossier `worlds` à la racine de Luanti. Si le dossier `worlds` n'existe pas, il faut le créer
2. Renommer le dossier si nécessaire. Exemple : `carte_seine_saint_denis`
3. À l'intérieur du dossier de la carte, créer un sous-dossier `worldmods` dans lequel vous pourrez y déposer tous vos "mods" (explication dans la suite de ce tutoriel).
---

## 2. Installation et configuration du serveur

Il faut prendre en compte le fait que l'installation d'un serveur, peu importe la manière, mérite certaines compétences informatiques. Si cela vous est impossible, vous pouvez toujours installer Luanti sur votre propre ordinateur et vous en servir comme serveur local. 

![Schema serveur Luanti](/images/photos/unej/schema_serveur_luanti.jpg)


Pour lancer une partie locale le plus simplement possible, il vous faut [télécharger le client Luanti](https://luanti.fr/downloads/) qui correspond à votre système d'exploitation puis suivre les étapes suivantes :

1. Déposez la carte dans le dossier `worlds` du dossier Luanti que vous aurez téléchargé
2. Lancer le client Luanti et cliquez sur `Démarrer`
3. Sélectionnez vos mods manuellement
4. Activez le mode créatif et l'hébergement du serveur
5. Entrez un nom et un mot de pas administrateur
6. Cliquez sur `Héberger une partie`

![Schema serveur Luanti](/images/photos/unej/partie_locale_luanti.jpg)

N'importe quelle personne connectée au même réseau internet que vous, pourras rejoindre votre en cliquant sur `Rejoindre une partie` puis en entrant votre [adresse IPV4](https://www.mon-ip.com/) comme adresse de serveur

### 2.1 Installation des services MinetestLab (Modifications à venir)

Dans le cadre de UNEJ, l'IRI a mis ensemble un certains nombre de services proposés par la communauté Luanti permettant la mise en place d'un serveur orienté activités éducatives et gestion d'élèves. Tous ces outils ont été réunis dans un serveur intitulé **MinetestLab (MTL)**

Le serveur MTL repose sur une image **[Docker](https://docs.docker.com/)**, facilitant son déploiement et sa gestion. Cette image contient tous les éléments nécessaires pour exécuter un serveur Minetest/Luanti préconfiguré. 

Vous pouvez télécharger et utiliser cette image en accédant au dépôt suivant :  

[MinetestLab Docker Image](https://forge.apps.education.fr/iri/minetest/mtl-server/-/tree/main/docker/home?ref_type=heads)

Ce dossier contient la définition de la page d'accueil du serveur MTL.  
Cette page utilise **Hugo**, un générateur de sites web statiques.  

La page est construite dans le **Dockerfile**, qui est appelé par le projet **docker-compose.yaml** situé dans le dossier parent.  

Si vous avez les compétences de développement nécessaires, le site web peut être facilement construit **localement** ou en utilisant un **conteneur** (voir les détails sur le dépôt d'origine).  

#### Téléchargerz l'image Docker  

Récupérez l’image Docker contenant **Hugo** et ses extensions :

```sh
docker pull hugomods/hugo:exts
```
#### Installez les dépendances avec le conteneur

Exécutez cette commande pour installer les dépendances du projet directement dans le conteneur :

```
docker run --rm -v $(pwd):/src -w /src hugomods/hugo:exts npm ci
```

#### Générez la page d’accueil en exécutant la commande suivante :

```
docker run --rm -v $(pwd):/src -w /src hugomods/hugo:exts hugo
```

### 2.2 Le serveur Luanti (Modifications à venir)

Une fois l'image installé, vous aurez accès à une page web vous permettant d'administrer votre serveur en ligne.

L'arborescence Luanti est constituée de plusieurs dossiers importants :
- `games` : contient les jeux installés (ici c'est le jeu `mtl` qui est installé automatiquement  : [mtl_game](https://gitlab.com/iri-research-org/urbanum/minetest/mtl_game)). "À noter qu'un jeu constitue un ensemble de mods compatibles entre eux et agencés selon des règles définies par son créateur"
- `mods` : contient les mods globaux du serveur (à ne pas toucher).
- `worlds` : contient les mondes, y compris la carte IGN installée précédemment.
- `worlds/<NomCarte>/worldmods` : contient les mods spécifiques à la carte.
- `minetest.conf`: fichier de configuration du serveur à compléter ([exemple ici](https://github.com/luanti-org/luanti/blob/master/minetest.conf.example))

### 2.3 Les mods

*Dans le cadre de Luanti, un "mod" est une modification qui permet d'ajouter de nouveaux éléments, fonctionnalités, ou comportements au jeu. Cela peut inclure des blocs, objets, textures, mécanismes, ou encore des modifications du gameplay. Ces mods sont écrits principalement en Lua, un langage de programmation léger, et peuvent être partagés ou téléchargés via la [plateforme officielle](https://content.luanti.org/) ou des forums communautaires.

Les mods dans Luanti permettent de personnaliser et d'étendre le monde voxel, en offrant une variété d'options qui vont des changements simples, comme ajouter un nouveau type de matériau, à des ajouts plus complexes, tels que des mécaniques de jeu entièrement nouvelles (par exemple, des systèmes de survie, des interactions entre joueurs, ou même des outils de gestion de serveur).

*Durant les ateliers UNEJ, nous avons choisi une collections de mods nommé Edu Mods visible sur la plateforme officielle : [https://content.luanti.org/collections/IRI/edu_mods/](https://content.luanti.org/collections/IRI/edu_mods/)*

Les mods suivants sont directement installés dans le jeu mtl et sont essentiels pour une administration minimale du serveur :

- [areas](https://github.com/minetest-mods/areas)
- [default](https://github.com/minetest-game-mods/default)
- [server_news](https://github.com/Ezhh/server_news)
- [mapserver_mod](https://github.com/minetest-mapserver/mapserver_mod)
- [maptools](https://github.com/minetest-mods/maptools)
- [monitoring](https://github.com/minetest-monitoring/monitoring)
- [playerfactions](https://github.com/mt-mods/playerfactions)
- [playerfactions_ext](https://forge.apps.education.fr/iri/minetest/playerfactions_ext)
- [we_undo](https://github.com/HybridDog/we_undo)
- [whitelist](https://github.com/AntumMT/mod-whitelist.git)
- [worldedit](https://github.com/Uberi/Minetest-WorldEdit)
- [worldedit_bigschems](https://forge.apps.education.fr/iri/minetest/worldedit_bigschems)

Par la suite, nous vous conseillons d'utiliser spécifique les mod de la collection Edu Mods, mais si vous vous en sentez capable, n'hésitez pas explorer tous les mods de la communauté.

Une fois un mod téléchargé, pour que celui-ci soit opérant dans votre monde, il faut le déposer dans le dossier `worldmods` de votre monde.

---

## 3. Connexion au serveur

### 3.1 Utilisation du client
Deux options sont possibles :

1. **Client web** : [Client Luanti Web](https://minetest.dustlabs.io/) (Onglet "Public Servers").

Ce client vous permet de jouer à Luanti sans avoir à télécharger quoi que ce soit. Malgré quelques ralentissements, vous pourrez ainsi éviter les problématiques de réseau et/ou de sécurité liées au matériel informatique que vous uitlisez. Cependant, vous ne pourrez pas conserver les configurations de votre Client comme la personnalisation de vos touches de clavier ou les paramètres graphiques par exemple.

2. **Client natif** : [Téléchargement du client Luanti](https://luanti.fr/downloads/).

Ce client est le plus stable et vous permet d'accéder à toute l'arborescence de Luanti en local et conserver vos configurations. Vous devrez néanmoins vous assurez que l'infrastructure réseau du lieu dans lequel vous menez vos ateliers vous permet de vous connecter à un serveur distant sans aucune contraine.

### 3.2 Connexion
1. Ouvrez le client Luanti et cliquez sur `Rejoindre une partie`
2. Entrer l'adresse du serveur et le port.
3. Renseignez un pseudo et un mot de passe.

---

## 4. Gestion des groupes et permissions

Dans le cadre de vos ateliers, vous allez devoir gérer des groupes d'élèves (ou autre type de joueurs) et modérer leurs actions selon ce que vous souhaitez leur faire construire ou visiter. Voici les commandes nécessaires pour pouvoir au maximum être en contrôle de votre groupe d'élèves. Comme exemple, nous prendrons un groupe d'élèves du collège Poincaré avec un élève ayant le pseudo *RiwadS* et un enseignant ayant le pseudo *MisterB*

*Rappel, pour exécuter une commande il faut ouvrir le chat en appuyant sur la touche T. Toutes les commandes commencent par `/` sauf les commandes worldedit qui commencent par `//`. Pour exécuter une commande vous devez vous assurer d'avoir le privilège correspondant. Pour regarder vos privilèges, il faut exécuter la commande `/privs MisterB`*

Pour voir toutes les commandes disponibles : `/help`

### 4.1 Gestion des groupes

Dans le cadre du projet UNEJ, nous avions utilisé le mod [playerfactions](https://content.luanti.org/packages/mt-mods/playerfactions/)
et avons développer un [mod ajoutant des fonctions supplémentaires](https://content.luanti.org/packages/IRI/playerfactions_ext/)

*Dans les jeux vidéo, une **faction** désigne généralement un groupe de joueurs réunis autour d’un objectif commun, souvent avec des mécaniques de compétition ou de combat. Cependant, dans le cadre de ce tutoriel, les factions sont uniquement utilisées comme un système de gestion de groupes. Elles permettent d'organiser les joueurs sans notion de rivalité, le jeu étant en mode créatif.*

Nous prendrons ici l'exemple d'un groupe nommé *6e_Poincaré* avec comme mot de passe du groupe *UNEJ93*

- Créer un groupe : `/factions create 6e_Poincaré UNEJ93`
- Ajouter un joueur : `/factions invite RiwadS 6e_Poincaré`
- Rejoindre un groupe : `/factions join 6e_Poincaré UNEJ93`
- Modifier le mot de passe : `/factions passwd New_UNEJ93 6e_Poincaré`
- Retirer un joueur : `/factions kick RiwadS`
- Voir la liste des groupes : `/factions list`
- Infos sur un groupe : `/factions info 6e_Poincaré`

### 4.2 Gestion des zones

La gestion de zone se fait avec le mod [areas](https://content.luanti.org/packages/ShadowNinja/areas/)

1. **Définir les coins de la zone**  
   - `/area_pos set` puis cliquer sur les deux coins à protéger.
   - `/area_pos1` ou `/area_pos2` pour définir un coin à votre position actuelle.
   - `/area_pos1 X Y Z` ou `/area_pos2 X Y Z` pour définir un coin aux coordonnées spécifiées.

2. **Protéger une zone**  
   - `/protect <NomZone>` → Protéger une zone pour soi (si l’auto-protection est activée).
   - `/set_owner <Joueur> <NomZone>` → Protéger une zone pour un joueur spécifique (nécessite le privilège `areas`).

3. **Gérer les droits d’accès**  
   - `/add_owner <IDZone> <Joueur> <NomSousZone>` → Accorder à un joueur le contrôle d’une sous-zone.
   - `/area_open <IDZone>` → Ouvrir/fermer la zone à tout le monde.
   - `/area_faction_open <IDZone> <Faction>` → Ouvrir/fermer la zone aux membres d’une faction.

4. **Autres commandes utiles**  
   - `/list_areas` → Lister les zones que vous possédez.
   - `/rename_area <IDZone> <NouveauNom>` → Renommer une zone.
   - `/remove_area <IDZone>` → Supprimer une zone (les sous-zones restent accessibles).
   - `/area_info` → Voir les informations d’une zone.
   - `/select_area <IDZone>` → Sélectionner une zone existante pour la modifier.


### 4.3 Gestion des privilèges

Les privs (ou privilèges) sont des permissions qui déterminent ce qu'un joueur peut faire sur un serveur. Par exemple, certains privs permettent de voler (fly), construire et interagir (interact), ou encore donner des objets (give).

Les administrateurs du serveur peuvent attribuer ou retirer ces privilèges aux joueurs avec des commandes comme :

`/grant <Joueur> <priv>` → Ajouter un privilège
`/revoke <Joueur> <priv>` → Retirer un privilège
`/privs <Joueur>` → Voir les privilèges d’un joueur

Ces permissions permettent de contrôler l'expérience de jeu et d'assurer un bon fonctionnement du serveur.

Avec le mod [playerfactions_ext](https://content.luanti.org/packages/IRI/playerfactions_ext/) qui étend les fonctions du mod playerfaction, il est possible d'utiliser les commandes `/grant` et `/revoke` sur un grouper entier

Par exemple, la commande `/revoke 6e_Poincaré fly` retire le privilège *fly* à tous les membres du groupe 6e_Poincaré

Quelques exemples supplémentaires de privilèges : 

1. **Privilèges de base**
- `interact` : Construire et miner
- `shout` : Parler dans le chat
- `fly` : Voler
- `fast` : Aller vite
- `noclip` : Traverser les blocs

2. **Privilèges de modération**
- `privs` : Accorder ou retirer des privilèges avec les commandes `/grant` et `/revoke`
- `ban` : Bannir un joueur
- `kick` : Expulser un joueur temporairement
- `teleport` : Utiliser la commande `/teleport JoueurOuGroupe Joueur2` pour téléporter un joueur ou un groupe vers un autre joueur
-  `give` : Utiliser la commande `/give NomJoueurouGroupe Objet:Nombre`  pour donner des objets à un joueur ou un groupe

3. **Privilèges avancés**
- `areas` : Gérer des protections de zones
- `playerfactions_admin` : Gérer et contrôler tous les groupes du serveur
- `worldedit` : Utiliser les [commandes de manipulation du monde](https://github.com/Uberi/Minetest-WorldEdit/blob/master/ChatCommands.md) commençant par `//`

Pour voir tous les privilèges disponibles, tapez la commande `/help privs`

---

## 5. Les blocs

Les blocs (appelés nodes) constitue l'élément de base pour la construction dans Luanti. Ils sont très nombreux (en plus des objets appelés items) et de nouveaux peuvent être créés par des contributeurs extérieurs. Il ne s'agit donc pas ici de faire une liste exhaustive des blocs du jeu (ce qui est impossible). Dans le cadre des ateliers, nous avons tenté de classer les sont classés en catégories pour correspondre à des matériaux réels :

### 5.1 Construction
- **Pierre** : `Stone (gris)`, `Sandstone (Blanc, Beige)`
- **Brique** : `Brick block (Rouge)`
- **Bois** : `Wood (4 couleurs)`

### 5.2 Espaces extérieurs
- **Sol plastique** : `Wool (plusieurs couleurs)`
- **Mobilier** : `Fence (toutes les couleurs)`, `Chair (toutes les couleurs)`

### 5.3 Éléments naturels
- **Arbres** : `Tree (toutes les occurrences)`
- **Herbes** : `Dirt with Grass`

### 5.4 Énergie
- **Éolienne** : `Windmill`
- **Énergie électrique** : [Mesecons](https://mesecons.net/items.html)

Vous pouvez évidemment créer votre propre référentiel.


***Très bons ateliers et n'hésitez-pas à nous contacter pour plus d'information !***

---
